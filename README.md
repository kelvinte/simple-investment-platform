### How to run
* The application is written using Java 17 and Spring boot 3.2.0
* Database Migration is in place using liquibase 
* Make sure you have Maven installed
* Install MSSQL (use docker azure-sql if you are on macOS), create a database named "kilde"
* Update .env file with the correct database credentials and other settings if needed
* Run the following commands
```
mvn spring-boot:run
```
* Go to [How to test](#How to test) Section
* default admin credentials: admin@admin.com/hello123

### Documentation
1. ER Diagram can be found in the root folder of the project (erdiagram.png)
2. Open API documentation can be found in the root folder of the project (apispec.yaml)
3. Postman collection can be found in the root folder of the project (kilde.postman_collection.json)

### How to test
* Login as an admin
```
POST http://localhost:8080/login
{
    "email":"admin@admin.com",
    "password":"hello123"
}
```
* Copy the access token
* Create a new tranche - paste the access token in the header
```
POST http://localhost:8080/admin/tranche
Authorization: Bearer <token> 
{
    "name":"Tranch A-3",
    "annualInterestRateInPercent": "5",
    "minimumInvestmentAmount": "500",
    "maximumInvestmentAmount": "1000000",
    "maximumInvestmentAmountPerInvestor":"3000",
    "durationInMonths": 24
}
```
* Register a new user
```
POST http://localhost:8080/register
{
    "name": "New User",
    "email": "newuser@newuser.com",
    "password": "newpassword"
}
```
* Login as new user
* View List of Tranch
```
GET http://localhost:8080/tranches
```
* View Tranche Details
```
GET http://localhost:8080/tranches/{id}
```
* Invest in a Tranche
```
POST http://localhost:8080/tranches/investment
{
    "trancheId: {trancheId},
    "amount": 1000
}
```
* Lock a Tranche (use Admin access token)
```
PATCH http://localhost:8080/admin/tranche/{trancheId}/lock
```
* USER's will not be able to invest in a LOCKED Tranche
* View Repayment schedule
```
GET http://localhost:8080/tranches/{trancheId}
```
* Pay a Tranche (use Admin access token)
```
POST http://localhost:8080/admin/tranche/{trancheId}/repayment/{repaymentId}
```
* View Repayment schedule again to see the updated status
* View User's Investment (use User access token)
```
GET http://localhost:8080/investor
```



### Assumptions
1. Annual Interest rate is limited to 2 decimal place in percentage ex: 6.38%
2. A tranche can be invested by different investors
3. A tranche can be invested multiple times by the same investor
4. A tranche repayment can be calculated upon creation
5. 2% is the default fee for the investment, hence 98% of the investment amount is added to the tranche
6. The return of the investment is calculated based on the amount invested and the interest rate of the tranche
7. The monthly return will only be calculated upon tranche payment
8. A tranche can only be invested when it is in OPEN state
9. A tranche can only be repaid when it is in LOCKED state

There are 2 Roles available in the application - ADMIN and USER
1. ADMIN can create a new tranche
2. ADMIN can lock the tranche
3. ADMIN can pay the tranche (assumption: payment can be done via offline way such as pay by cheque, bank transfer, etc)
4. User/Admin can view the list of tranche
5. User/Admin can view the tranche details
6. User/Admin can invest in a tranche
7. New user registration's role is set to USER as default
