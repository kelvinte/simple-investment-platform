package sg.kilde.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimpleInvestmentPlatformApplication {

	public static void main(String[] args) {
		SpringApplication.run(SimpleInvestmentPlatformApplication.class, args);
	}

}
