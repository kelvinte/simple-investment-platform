package sg.kilde.app.adapter.dto.auth;

import jakarta.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class LoginRequest {
  @NotEmpty(message = "Email cannot be empty")
  private String email;
  @NotEmpty(message = "Password cannot be empty")
  private String password;
}
