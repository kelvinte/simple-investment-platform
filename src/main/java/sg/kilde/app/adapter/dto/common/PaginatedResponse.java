package sg.kilde.app.adapter.dto.common;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class PaginatedResponse <T> {
  private List<T> result;
  private Integer page;
  private Integer size;
  private Integer totalPage;
  private Long totalItems;
}
