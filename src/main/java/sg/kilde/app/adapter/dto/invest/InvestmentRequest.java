package sg.kilde.app.adapter.dto.invest;

import jakarta.validation.constraints.NotNull;
import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class InvestmentRequest {
  @NotNull(message = "Investment amount is required")
  private BigDecimal amount;
  @NotNull(message = "Tranche id is required")
  private Long trancheId;
}
