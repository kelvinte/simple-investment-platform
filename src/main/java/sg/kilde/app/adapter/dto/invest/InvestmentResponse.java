package sg.kilde.app.adapter.dto.invest;

import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import sg.kilde.app.adapter.dto.tranche.TrancheResponse;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class InvestmentResponse {
  private BigDecimal amount;
  private TrancheResponse tranche;
  private Long investmentId;
}
