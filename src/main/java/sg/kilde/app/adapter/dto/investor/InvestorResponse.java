package sg.kilde.app.adapter.dto.investor;

import lombok.*;
import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor

public class InvestorResponse {
    private Long id;
    private String email;
    private String name;
    private String status;
    private String role;
    private List<InvestorTrancheResponse> investments;
}
