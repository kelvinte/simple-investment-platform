package sg.kilde.app.adapter.dto.investor;

import lombok.*;
import sg.kilde.app.adapter.dto.invest.InvestmentResponse;
import sg.kilde.app.adapter.dto.tranche.TrancheResponse;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class InvestorTrancheResponse {
    private TrancheResponse tranche;
    private List<InvestmentResponse> investments;
}
