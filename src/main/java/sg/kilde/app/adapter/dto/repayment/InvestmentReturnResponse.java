package sg.kilde.app.adapter.dto.repayment;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class InvestmentReturnResponse {
    private Long id;
    private BigDecimal amount;
    private Integer monthOrdinal;
    private Long investmentId;
}
