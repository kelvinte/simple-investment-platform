package sg.kilde.app.adapter.dto.repayment;

import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RepaymentResponse {
  private Long id;
  private Integer month;
  private BigDecimal amountToPay;
  private Boolean paid;
}
