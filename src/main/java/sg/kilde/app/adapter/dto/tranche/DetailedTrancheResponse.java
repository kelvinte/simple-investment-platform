package sg.kilde.app.adapter.dto.tranche;

import lombok.*;
import sg.kilde.app.adapter.dto.repayment.RepaymentResponse;

import java.util.List;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class DetailedTrancheResponse {
    private TrancheResponse tranche;

    private List<RepaymentResponse> repayments;
}
