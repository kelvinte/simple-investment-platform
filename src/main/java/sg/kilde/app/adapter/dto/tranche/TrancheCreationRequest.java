package sg.kilde.app.adapter.dto.tranche;

import jakarta.validation.constraints.DecimalMin;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class TrancheCreationRequest {
    @NotEmpty(message = "Tranche name cannot be empty")
    private String name;
    @NotNull(message = "Annual Interest Rate cannot be empty")
    @DecimalMin(value = "0.001", message = "Minimum annual interest rate is 0.001%")
    private BigDecimal annualInterestRateInPercent;
    @NotNull(message = "Minimum investment amount cannot be empty")
    private BigDecimal minimumInvestmentAmount;
    @NotNull(message = "Maximum investment amount cannot be empty")
    private BigDecimal maximumInvestmentAmount;
    @NotNull(message = "Maximum investment amount per person cannot be empty")
    private BigDecimal maximumInvestmentAmountPerInvestor;
    private Integer durationInMonths;
}
