package sg.kilde.app.adapter.dto.tranche;

import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class TrancheResponse {
  private int id;

  private String name;

  private BigDecimal annualInterestRate;

  private BigDecimal availableAmount;

  private BigDecimal minimumInvestmentAmount;

  private BigDecimal maximumInvestmentAmount;

  private BigDecimal maximumInvestmentAmountPerInvestor;

  private int durationInMonths;
  private String status;
}
