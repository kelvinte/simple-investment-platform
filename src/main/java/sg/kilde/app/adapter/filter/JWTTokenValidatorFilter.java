package sg.kilde.app.adapter.filter;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import javax.crypto.SecretKey;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import sg.kilde.app.application.JWTTokenService;
import sg.kilde.app.common.constant.Constants;
import sg.kilde.app.common.constant.ErrorCode;
import sg.kilde.app.common.constant.UrlConstants;
import sg.kilde.app.domain.repository.AppUserRepository;
import sg.kilde.app.domain.user.LoggedInUser;

@Component
public class JWTTokenValidatorFilter extends OncePerRequestFilter {

  @Value("${jwt.secret}")
  private String jwtSecretKey;

  private final JWTTokenService jwtTokenService;
  private final AppUserRepository appUserRepository;

  public JWTTokenValidatorFilter(JWTTokenService jwtTokenService,
      AppUserRepository appUserRepository) {
    this.jwtTokenService = jwtTokenService;
    this.appUserRepository = appUserRepository;
  }

  @Override
  protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response,
      FilterChain filterChain) throws ServletException, IOException {
    boolean isAuth = request.getServletPath().equals(UrlConstants.LOGIN) || request.getServletPath()
            .equals(UrlConstants.REGISTER);
    if(isAuth){
      filterChain.doFilter(request, response);
      return;
    }
    String jwt = request.getHeader(Constants.JWT_HEADER);
    if (jwt == null ||!jwt.startsWith(Constants.BEARER)) {
      filterChain.doFilter(request, response);
      return;
    }

    jwt = jwt.substring(6).trim();

    try {
      SecretKey key = Keys.hmacShaKeyFor(
          jwtSecretKey.getBytes(StandardCharsets.UTF_8));

      Claims claims = Jwts.parserBuilder()
          .setSigningKey(key)
          .build()
          .parseClaimsJws(jwt)
          .getBody();
      String email = jwtTokenService.extractUsername(jwt);
      String authorities = (String) claims.get(Constants.AUTHORITY_CLAIM);
      Long userId = Long.parseLong((String)claims.get(Constants.USER_ID_CLAIM));

      var appUser = appUserRepository.findByEmail(email).orElseThrow(() -> new BadCredentialsException(
          ErrorCode.INVALID_TOKEN.getMessage()));

      if(!appUser.getId().equals(userId)){
        throw new BadCredentialsException(ErrorCode.INVALID_TOKEN.getMessage());
      }

      Authentication auth = new LoggedInUser(userId, email, null,
          AuthorityUtils.commaSeparatedStringToAuthorityList(authorities));
      SecurityContextHolder.getContext().setAuthentication(auth);
      filterChain.doFilter(request, response);
    } catch (Exception e) {
      throw new BadCredentialsException(ErrorCode.INVALID_TOKEN.getMessage(), e);
    }

  }

}
