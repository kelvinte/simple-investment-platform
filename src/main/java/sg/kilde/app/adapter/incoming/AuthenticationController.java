package sg.kilde.app.adapter.incoming;


import jakarta.validation.Valid;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import sg.kilde.app.adapter.dto.auth.LoginRequest;
import sg.kilde.app.adapter.dto.auth.LoginResponse;
import sg.kilde.app.adapter.dto.auth.RegistrationRequest;
import sg.kilde.app.application.AuthenticationService;
import sg.kilde.app.common.constant.UrlConstants;

@RestController
public class AuthenticationController {

  private final AuthenticationService authenticationService;

  public AuthenticationController(AuthenticationService authenticationService) {
    this.authenticationService = authenticationService;
  }

  @PostMapping
  @RequestMapping(path = UrlConstants.REGISTER)
  public void register(@RequestBody @Valid RegistrationRequest registrationRequest) {
    authenticationService.register(registrationRequest);
  }

  @PostMapping
  @RequestMapping(path = UrlConstants.LOGIN)
  public LoginResponse login(@RequestBody @Valid LoginRequest loginRequest) {
    return authenticationService.login(loginRequest);
  }
}
