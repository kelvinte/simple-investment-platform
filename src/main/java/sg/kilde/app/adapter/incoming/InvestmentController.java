package sg.kilde.app.adapter.incoming;

import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import sg.kilde.app.adapter.dto.invest.InvestmentRequest;
import sg.kilde.app.adapter.dto.invest.InvestmentResponse;
import sg.kilde.app.application.InvestmentService;
import sg.kilde.app.common.constant.UrlConstants;

@Slf4j
@RestController
@RequestMapping(path = UrlConstants.INVESTMENT)
public class InvestmentController {

  private final InvestmentService investmentService;

  public InvestmentController(InvestmentService investmentService) {
    this.investmentService = investmentService;
  }


  @PostMapping
  public InvestmentResponse invest(@RequestBody @Valid InvestmentRequest investmentRequest){
    log.info("Investing on tranche {} with amount {}", investmentRequest.getTrancheId(),
        investmentRequest.getAmount());
    return this.investmentService.invest(investmentRequest);
  }
}
