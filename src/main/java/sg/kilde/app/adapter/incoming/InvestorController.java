package sg.kilde.app.adapter.incoming;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import sg.kilde.app.adapter.dto.investor.InvestorResponse;
import sg.kilde.app.application.InvestorService;
import sg.kilde.app.common.constant.UrlConstants;

@Slf4j
@RestController
@RequestMapping(path = UrlConstants.INVESTOR)
public class InvestorController {

  private final InvestorService investorService;

  public InvestorController(InvestorService investorService) {
    this.investorService = investorService;
  }

  @GetMapping
  public InvestorResponse getInvestorDetails(){
    log.info("Getting investor Detail");
    return investorService.getInvestorDetails();
  }
}
