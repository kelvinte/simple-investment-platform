package sg.kilde.app.adapter.incoming;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import sg.kilde.app.adapter.dto.repayment.RepaymentResponse;
import sg.kilde.app.application.RepaymentService;
import sg.kilde.app.common.constant.UrlConstants;

@Slf4j
@RestController
public class RepaymentController {

    private final RepaymentService repaymentService;

    public RepaymentController(RepaymentService repaymentService) {
        this.repaymentService = repaymentService;
    }

    @PostMapping(path = UrlConstants.ADMIN_TRANCHE + "/{trancheId}/repayment/{repaymentId}")
    public RepaymentResponse pay(@PathVariable("trancheId") Long trancheId, @PathVariable("repaymentId") Long repaymentId){
        log.info("Initiating repayment response");
        return this.repaymentService.pay(trancheId, repaymentId);
    }

}
