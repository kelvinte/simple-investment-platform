package sg.kilde.app.adapter.incoming;

import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import sg.kilde.app.adapter.dto.common.PaginatedResponse;
import sg.kilde.app.adapter.dto.tranche.DetailedTrancheResponse;
import sg.kilde.app.adapter.dto.tranche.TrancheCreationRequest;
import sg.kilde.app.adapter.dto.tranche.TrancheResponse;
import sg.kilde.app.application.TrancheService;
import sg.kilde.app.common.constant.UrlConstants;
@Slf4j
@RestController
@RequestMapping
public class TrancheController {

  private final TrancheService trancheService;

  public TrancheController(TrancheService trancheService) {
    this.trancheService = trancheService;
  }

  @GetMapping(path = UrlConstants.TRANCHE)
  public PaginatedResponse<TrancheResponse> getTranches(
      @RequestParam(value = "page", defaultValue = "0") int page,
      @RequestParam(value = "size", defaultValue = "10") int size){
    log.info("Get List of Tranche");
    return this.trancheService.getPaginatedTranches(page, size);
  }
  @GetMapping(path = UrlConstants.TRANCHE+"/{id}")
  public DetailedTrancheResponse getTranche(@PathVariable(value = "id") Long id){
    log.info("Get Tranche");
    return this.trancheService.getTranche(id);
  }


  @PostMapping(path = UrlConstants.ADMIN_TRANCHE)
  public DetailedTrancheResponse createTranche(@RequestBody @Valid TrancheCreationRequest creationRequest){
    log.info("Tranche Created");
    return this.trancheService.createTranche(creationRequest);
  }


  @PatchMapping(path = UrlConstants.ADMIN_TRANCHE+"/{id}/lock")
  public void lockTranche(@PathVariable(value = "id") Long id){
    log.info("Lock Tranche");
    this.trancheService.lockTranche(id);
  }
}
