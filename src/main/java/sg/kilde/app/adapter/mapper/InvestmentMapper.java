package sg.kilde.app.adapter.mapper;

import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import sg.kilde.app.adapter.dto.invest.InvestmentResponse;
import sg.kilde.app.common.utils.MathUtils;
import sg.kilde.app.domain.entity.Investment;
import sg.kilde.app.domain.entity.InvestmentReturn;

import java.util.List;

@Mapper(componentModel = "spring",
    uses = {TrancheMapper.class, InvestmentReturnMapper.class},
    imports = {MathUtils.class},
    injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface InvestmentMapper {


  @Mapping(target = "investmentId", source = "investment.id")
  @Mapping(target = "amount", expression = "java(MathUtils.round(investment.getAmount(),2))")
  InvestmentResponse convert(Investment investment);

  List<InvestmentResponse> convert(List<Investment> investments);
}
