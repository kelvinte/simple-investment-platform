package sg.kilde.app.adapter.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import sg.kilde.app.adapter.dto.repayment.InvestmentReturnResponse;
import sg.kilde.app.common.utils.MathUtils;
import sg.kilde.app.domain.entity.InvestmentReturn;

@Mapper(componentModel = "spring", imports = {MathUtils.class})
public interface InvestmentReturnMapper {

    @Mapping(target = "investmentId", source = "investment.id")
    @Mapping(target = "amount", expression = "java(MathUtils.round(investmentReturn.getAmount(),2))")
    InvestmentReturnResponse convert(InvestmentReturn investmentReturn);
}
