package sg.kilde.app.adapter.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.beans.factory.annotation.Autowired;
import sg.kilde.app.adapter.dto.investor.InvestorResponse;
import sg.kilde.app.adapter.dto.investor.InvestorTrancheResponse;
import sg.kilde.app.domain.entity.AppUser;
import sg.kilde.app.domain.entity.Investment;
import sg.kilde.app.domain.entity.Tranche;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class InvestorMapper {

    @Autowired
    protected TrancheMapper trancheMapper;

    @Autowired
    protected InvestmentMapper investmentMapper;

    public InvestorResponse toInvestorResponse(AppUser user, List<Investment> investments){

        Map<Tranche, List<Investment>> map =
                investments.stream().collect(Collectors.groupingBy(Investment::getTranche));

        List<InvestorTrancheResponse> investorTrancheResponses = new ArrayList<>();
        for(Map.Entry<Tranche, List<Investment>> entry : map.entrySet()){
            var tranche = entry.getKey();
            var investmentsList = entry.getValue();
            var trancheResponse = convert(tranche, investmentsList);
            investorTrancheResponses.add(trancheResponse);
        }

        return InvestorResponse.builder()
                .id(user.getId())
                .email(user.getEmail())
                .role(user.getAppRole().getName())
                .name(user.getName())
                .status(user.getStatus().name())
                .investments(investorTrancheResponses)
                .build();
    }


    @Mapping(target = "tranche", expression = "java(trancheMapper.convert(tranche))")
    @Mapping(target = "investments", expression = "java(investmentMapper.convert(investments))")
    public abstract InvestorTrancheResponse convert(Tranche tranche, List<Investment> investments);
}
