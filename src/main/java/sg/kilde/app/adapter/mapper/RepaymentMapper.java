package sg.kilde.app.adapter.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import sg.kilde.app.adapter.dto.repayment.RepaymentResponse;
import sg.kilde.app.common.utils.MathUtils;
import sg.kilde.app.domain.entity.Repayment;

@Mapper(componentModel = "spring", imports = MathUtils.class)
public interface RepaymentMapper {

    @Mapping(target = "month", source = "monthOrdinal")
    @Mapping(target = "amountToPay", expression = "java(MathUtils.round(repayment.getAmount(),2))")
    RepaymentResponse convert(Repayment repayment);
}
