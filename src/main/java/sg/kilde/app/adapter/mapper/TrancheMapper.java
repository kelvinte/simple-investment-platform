package sg.kilde.app.adapter.mapper;

import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import sg.kilde.app.adapter.dto.tranche.DetailedTrancheResponse;
import sg.kilde.app.adapter.dto.tranche.TrancheCreationRequest;
import sg.kilde.app.adapter.dto.tranche.TrancheResponse;
import sg.kilde.app.common.utils.MathUtils;
import sg.kilde.app.domain.entity.Repayment;
import sg.kilde.app.domain.entity.Tranche;

import java.util.List;

@Mapper(componentModel = "spring", imports = {MathUtils.class},
        uses = {RepaymentMapper.class},
        injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface TrancheMapper {

    @Mapping(target="annualInterestRate",
        expression = "java(MathUtils.convertPercentToDecimal(request.getAnnualInterestRateInPercent()))")
    @Mapping(target="availableAmount", source = "maximumInvestmentAmount")
    Tranche convert(TrancheCreationRequest request);

    TrancheResponse convert(Tranche tranche);

    @Mapping(target = "tranche", source = "tranche")
    @Mapping(target = "repayments", source = "repaymentList")
    DetailedTrancheResponse convertDetailed(Tranche tranche, List<Repayment> repaymentList);
}
