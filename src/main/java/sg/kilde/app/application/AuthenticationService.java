package sg.kilde.app.application;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import sg.kilde.app.adapter.dto.auth.LoginRequest;
import sg.kilde.app.adapter.dto.auth.LoginResponse;
import sg.kilde.app.adapter.dto.auth.RegistrationRequest;
import sg.kilde.app.common.constant.ErrorCode;
import sg.kilde.app.common.constant.RoleConstants;
import sg.kilde.app.common.constant.UserStatus;
import sg.kilde.app.common.exception.AppException;
import sg.kilde.app.domain.entity.AppRole;
import sg.kilde.app.domain.entity.AppUser;
import sg.kilde.app.domain.repository.AppUserRepository;

@Slf4j
@Service
public class AuthenticationService {

  private final AppUserRepository appUserRepository;
  private final PasswordEncoder passwordEncoder;
  private final AuthenticationManager authenticationManager;
  private final JWTTokenService jwtTokenService;

  public AuthenticationService(AppUserRepository appUserRepository, PasswordEncoder passwordEncoder,
                               AuthenticationManager authenticationManager, JWTTokenService jwtTokenService) {
    this.appUserRepository = appUserRepository;
    this.passwordEncoder = passwordEncoder;
    this.authenticationManager = authenticationManager;
    this.jwtTokenService = jwtTokenService;
  }

  public void register(RegistrationRequest registrationRequest){
    log.info("Registering {}", registrationRequest.getName());

    var user = appUserRepository.findByEmail(registrationRequest.getEmail());

    if(user.isPresent()){
      throw new AppException(ErrorCode.EMAIL_ALREADY_IN_USE);
    }

    AppRole appRole = AppRole.builder().id(RoleConstants.USER).build();

    AppUser appUser = AppUser.builder()
        .email(registrationRequest.getEmail())
        .password(passwordEncoder.encode(registrationRequest.getPassword()))
        .name(registrationRequest.getName())
        .status(UserStatus.ACTIVE)
        .appRole(appRole)
        .build();

    appUserRepository.save(appUser);
  }

  public LoginResponse login(LoginRequest loginRequest){
    var user = appUserRepository.findByEmail(loginRequest.getEmail()).orElseThrow(() -> new BadCredentialsException(
        ErrorCode.INVALID_USERNAME_OR_PASSWORD.getMessage()));
    authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginRequest.getEmail(), loginRequest.getPassword()));
    var jwtToken = jwtTokenService.generateToken(user);
    return LoginResponse.builder()
        .accessToken(jwtToken)
        .build();
  }

}
