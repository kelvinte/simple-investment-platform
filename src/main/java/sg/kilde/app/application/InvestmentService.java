package sg.kilde.app.application;

import java.math.BigDecimal;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import sg.kilde.app.adapter.dto.invest.InvestmentRequest;
import sg.kilde.app.adapter.dto.invest.InvestmentResponse;
import sg.kilde.app.adapter.mapper.InvestmentMapper;
import sg.kilde.app.application.investment.InvestmentProcess;
import sg.kilde.app.common.constant.ErrorCode;
import sg.kilde.app.common.constant.TrancheStatus;
import sg.kilde.app.common.exception.AppException;
import sg.kilde.app.domain.entity.Investment;
import sg.kilde.app.domain.entity.Tranche;
import sg.kilde.app.domain.repository.AppUserRepository;
import sg.kilde.app.domain.repository.InvestmentRepository;
import sg.kilde.app.domain.repository.TrancheRepository;

@Service
public class InvestmentService {

  private final TrancheRepository trancheRepository;
  private final AppUserRepository appUserRepository;
  private final InvestmentRepository investmentRepository;
  private final LoggedinUserService loggedinUserService;

  private final InvestmentProcess investmentProcess;
  private final InvestmentMapper investmentMapper;

  public InvestmentService(TrancheRepository trancheRepository,
      AppUserRepository appUserRepository,
      InvestmentRepository investmentRepository,
      LoggedinUserService loggedinUserService,
      InvestmentProcess investmentProcess,
      InvestmentMapper investmentMapper) {
    this.trancheRepository = trancheRepository;
    this.appUserRepository = appUserRepository;
    this.investmentRepository = investmentRepository;
    this.loggedinUserService = loggedinUserService;
    this.investmentProcess = investmentProcess;
    this.investmentMapper = investmentMapper;
  }

  @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
  public InvestmentResponse invest(InvestmentRequest request) {
    var tranche = this.trancheRepository.findById(request.getTrancheId()).orElseThrow(() ->
        new AppException(ErrorCode.TRANCHE_NOT_FOUND));
    validateInvestmentRequest(tranche, request);

    var loggedInUser = loggedinUserService.getLoggedInUser();
    var user = appUserRepository.findById(loggedInUser.getId()).orElseThrow(() ->
        new AppException(ErrorCode.USER_NOT_FOUND));

    Investment investment = Investment.builder()
        .amount(request.getAmount())
        .appUser(user)
        .tranche(tranche)
        .build();

    investment = investmentProcess.processInvestment(investment);
    investmentRepository.save(investment);

    var availableAmount = tranche.getAvailableAmount().subtract(investment.getAmount());
    tranche.setAvailableAmount(availableAmount);
    trancheRepository.save(tranche);

    return investmentMapper.convert(investment);
  }


  private void validateInvestmentRequest(Tranche tranche, InvestmentRequest investmentRequest) {
    if(tranche.getStatus() == TrancheStatus.LOCKED || tranche.getStatus() == TrancheStatus.DONE){
      throw new AppException(ErrorCode.TRANCHE_ALREADY_LOCKED);
    }
    if (investmentRequest.getAmount().compareTo(BigDecimal.ZERO) <= 0) {
      throw new AppException(ErrorCode.INVESTMENT_MUST_BE_GREATER_THAN_ZERO);
    }
    if (investmentRequest.getAmount().compareTo(tranche.getAvailableAmount()) > 0) {
      throw new AppException(ErrorCode.INVESTMENT_AMOUNT_GREATER_THAN_AVAILABLE_AMOUNT);
    }
    if (investmentRequest.getAmount()
        .compareTo(tranche.getMaximumInvestmentAmountPerInvestor()) > 0) {
      throw new AppException(ErrorCode.INVESTMENT_AMOUNT_GREATER_THAN_MAX_INVESTMENT_PER_INVESTOR);
    }

    if (investmentRequest.getAmount().compareTo(tranche.getMinimumInvestmentAmount()) < 0) {
      throw new AppException(ErrorCode.INVESTMENT_AMOUNT_LESS_THAN_MIN_INVESTMENT);
    }
  }
}

