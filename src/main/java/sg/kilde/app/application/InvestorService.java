package sg.kilde.app.application;

import org.springframework.stereotype.Service;
import sg.kilde.app.adapter.dto.investor.InvestorResponse;
import sg.kilde.app.adapter.mapper.InvestorMapper;
import sg.kilde.app.common.constant.ErrorCode;
import sg.kilde.app.common.exception.AppException;
import sg.kilde.app.domain.repository.AppUserRepository;
import sg.kilde.app.domain.repository.InvestmentRepository;

@Service
public class InvestorService {

    private final LoggedinUserService loggedinUserService;
    private final AppUserRepository appUserRepository;
    private final InvestmentRepository investmentRepository;

    private final InvestorMapper investorMapper;
    public InvestorService(LoggedinUserService loggedinUserService, AppUserRepository appUserRepository, InvestmentRepository investmentRepository, InvestorMapper investorMapper) {
        this.loggedinUserService = loggedinUserService;
        this.appUserRepository = appUserRepository;
        this.investmentRepository = investmentRepository;
        this.investorMapper = investorMapper;
    }

    public InvestorResponse getInvestorDetails(){
        var loggedInUser = loggedinUserService.getLoggedInUser();
        var user = appUserRepository.findById(loggedInUser.getId()).orElseThrow(() ->
                new AppException(ErrorCode.USER_NOT_FOUND));
        var investments = investmentRepository.findByAppUserId(user.getId());

        return investorMapper.toInvestorResponse(user, investments);
    }
}
