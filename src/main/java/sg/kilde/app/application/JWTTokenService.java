package sg.kilde.app.application;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.util.Date;
import java.util.Map;
import java.util.function.Function;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import sg.kilde.app.common.constant.Constants;
import sg.kilde.app.domain.entity.AppUser;

@Service
public class JWTTokenService {
  @Value("${jwt.secret}")
  private String jwtSecretKey;
  @Value("${jwt.expiration}")
  private long jwtExpiration;

  public String generateToken(AppUser user) {

    return Jwts
            .builder()
            .setClaims(
                    Map.of(Constants.AUTHORITY_CLAIM, user.getAppRole().getName(),
                          Constants.USER_ID_CLAIM, user.getId().toString())
            )
            .setSubject(user.getEmail())
            .setIssuedAt(new Date(System.currentTimeMillis()))
            .setExpiration(new Date(System.currentTimeMillis() + jwtExpiration))
            .signWith(getSignInKey(), SignatureAlgorithm.HS256)
            .compact();
  }
  public String extractUsername(String token) {
    return extractClaim(token, Claims::getSubject);
  }

  public <T> T extractClaim(String token, Function<Claims, T> claimsResolver) {
    final Claims claims = extractAllClaims(token);
    return claimsResolver.apply(claims);
  }

  private Claims extractAllClaims(String token) {
    return Jwts
        .parserBuilder()
        .setSigningKey(getSignInKey())
        .build()
        .parseClaimsJws(token)
        .getBody();
  }

  private Key getSignInKey() {
    return Keys.hmacShaKeyFor(jwtSecretKey.getBytes(StandardCharsets.UTF_8));
  }
}
