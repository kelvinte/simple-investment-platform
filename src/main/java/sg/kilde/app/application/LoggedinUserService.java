package sg.kilde.app.application;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import sg.kilde.app.domain.user.LoggedInUser;

@Service
public class LoggedinUserService {
  public LoggedInUser getLoggedInUser() {
    return (LoggedInUser) SecurityContextHolder.getContext().getAuthentication();
  }

}
