package sg.kilde.app.application;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import sg.kilde.app.adapter.dto.repayment.RepaymentResponse;
import sg.kilde.app.adapter.mapper.RepaymentMapper;
import sg.kilde.app.application.repayment.RepaymentCalculatorService;
import sg.kilde.app.common.constant.ErrorCode;
import sg.kilde.app.common.constant.TrancheStatus;
import sg.kilde.app.common.exception.AppException;
import sg.kilde.app.domain.entity.Investment;
import sg.kilde.app.domain.entity.InvestmentReturn;
import sg.kilde.app.domain.entity.Repayment;
import sg.kilde.app.domain.entity.Tranche;
import sg.kilde.app.domain.repository.InvestmentRepository;
import sg.kilde.app.domain.repository.InvestmentReturnRepository;
import sg.kilde.app.domain.repository.RepaymentRepository;
import sg.kilde.app.infrastracture.properties.AppFeeProperties;

import java.util.ArrayList;
import java.util.List;

@Service
public class RepaymentService {

    private final RepaymentRepository repaymentRepository;

    private final InvestmentRepository investmentRepository;
    private final RepaymentMapper repaymentMapper;

    private final InvestmentReturnRepository investmentReturnRepository;

    private final AppFeeProperties appFeeProperties;

    public RepaymentService(RepaymentRepository repaymentRepository, InvestmentRepository investmentRepository, RepaymentMapper repaymentMapper, InvestmentReturnRepository investmentReturnRepository, AppFeeProperties appFeeProperties) {
        this.repaymentRepository = repaymentRepository;
        this.investmentRepository = investmentRepository;
        this.repaymentMapper = repaymentMapper;
        this.investmentReturnRepository = investmentReturnRepository;
        this.appFeeProperties = appFeeProperties;
    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public RepaymentResponse pay(Long trancheId, Long repaymentId){

        var repayments = this.repaymentRepository.findByTrancheId(trancheId);

        if(repayments == null || repayments.isEmpty()){
            throw new AppException(ErrorCode.REPAYMENT_NOT_FOUND);
        }

        var repayment = repayments.stream()
            .filter(r -> r.getId().equals(repaymentId))
            .findFirst()
            .orElseThrow(() -> new AppException(ErrorCode.REPAYMENT_NOT_FOUND));


        Tranche tranche = repayment.getTranche();
        if(tranche.getStatus() != TrancheStatus.LOCKED){
            throw new AppException(ErrorCode.TRANCHE_NOT_LOCKED);
        }
        repayment.setPaid(true);

        if( isLastPayment(repayments, repayment)){
            tranche.setStatus(TrancheStatus.DONE);
        }

        this.repaymentRepository.save(repayment);


        List<Investment> investments = investmentRepository.findByTrancheId(tranche.getId());

        List<InvestmentReturn> investmentReturns = new ArrayList<>();
        investments.forEach(investment -> {
            RepaymentCalculatorService repaymentCalculatorService = RepaymentCalculatorService.getInstance(appFeeProperties.getBorrowerFeePercent());
            repaymentCalculatorService.calculate(tranche);
            repaymentCalculatorService.calculateLenderReturn(investment);
            var monthlyPayment = repaymentCalculatorService.getLenderReturn();

            var investmentReturn = new InvestmentReturn();
            investmentReturn.setInvestment(investment);
            investmentReturn.setAmount(monthlyPayment);
            investmentReturn.setMonthOrdinal(repayment.getMonthOrdinal());
            investmentReturns.add(investmentReturn);
        });
        investmentReturnRepository.saveAll(investmentReturns);

        return repaymentMapper.convert(repayment);
    }

    private boolean isLastPayment(List<Repayment> repayments, Repayment repayment){
        if(repayments.size() == 1){
            return true;
        }
        return repayments
            .stream()
            .filter(r->!(r.getId().equals(repayment.getId())))
            .allMatch(Repayment::getPaid);
    }
}
