package sg.kilde.app.application;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import sg.kilde.app.adapter.dto.common.PaginatedResponse;
import sg.kilde.app.adapter.dto.tranche.DetailedTrancheResponse;
import sg.kilde.app.adapter.dto.tranche.TrancheCreationRequest;
import sg.kilde.app.adapter.dto.tranche.TrancheResponse;
import sg.kilde.app.adapter.mapper.TrancheMapper;
import sg.kilde.app.application.repayment.RepaymentCalculatorService;
import sg.kilde.app.common.constant.ErrorCode;
import sg.kilde.app.common.constant.TrancheStatus;
import sg.kilde.app.common.exception.AppException;
import sg.kilde.app.domain.entity.Repayment;
import sg.kilde.app.domain.repository.RepaymentRepository;
import sg.kilde.app.domain.repository.TrancheRepository;
import sg.kilde.app.infrastracture.properties.AppFeeProperties;

@Slf4j
@Service
public class TrancheService {

    private final TrancheMapper trancheMapper;
    private final TrancheRepository trancheRepository;
    private final RepaymentRepository repaymentRepository;
    private final AppFeeProperties appFeeProperties;

    public TrancheService(TrancheMapper trancheMapper, TrancheRepository trancheRepository, RepaymentRepository repaymentRepository, AppFeeProperties appFeeProperties) {
        this.trancheMapper = trancheMapper;
        this.trancheRepository = trancheRepository;
        this.repaymentRepository = repaymentRepository;
        this.appFeeProperties = appFeeProperties;
    }
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public DetailedTrancheResponse getTranche(Long id){
        var tranche = trancheRepository.findById(id).orElseThrow(() -> new AppException(ErrorCode.TRANCHE_NOT_FOUND));
        return trancheMapper.convertDetailed(tranche, tranche.getRepayments());
    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public DetailedTrancheResponse createTranche(TrancheCreationRequest trancheCreationRequest){
        validateTrancheCreation(trancheCreationRequest);
        var tranche = trancheMapper.convert(trancheCreationRequest );
        tranche.setStatus(TrancheStatus.OPEN);
        trancheRepository.save(tranche);

        var calculatorService = RepaymentCalculatorService.getInstance(appFeeProperties.getBorrowerFeePercent());
        calculatorService.calculate(tranche);
        BigDecimal monthlyRepayment = calculatorService.getMonthlyRepayment();

        List<Repayment> repayments = new ArrayList<>();
        for (int i = 0; i < tranche.getDurationInMonths(); i++) {

            var repayment = new Repayment();
            repayment.setTranche(tranche);
            repayment.setMonthOrdinal(i+1);
            repayment.setAmount(monthlyRepayment);
            repayment.setPaid(false);
            repayments.add(repayment);
        }

        repaymentRepository.saveAll(repayments);

        return trancheMapper.convertDetailed(tranche, repayments);
    }


    private void validateTrancheCreation(TrancheCreationRequest trancheCreationRequest){
        if(trancheCreationRequest.getMinimumInvestmentAmount().compareTo(trancheCreationRequest.getMaximumInvestmentAmount()) > 0){
            throw new AppException(ErrorCode.MIN_INVESTMENT_GREATER_THAN_MAX_INVESTMENT);
        }
        if(trancheCreationRequest.getMaximumInvestmentAmountPerInvestor().compareTo(trancheCreationRequest.getMinimumInvestmentAmount()) < 0){
            throw new AppException(ErrorCode.MAX_INVESTMENT_PER_INVESTOR_LESS_THAN_MIN_INVESTMENT);
        }

    }

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public PaginatedResponse<TrancheResponse> getPaginatedTranches(int page, int size) {
        var tranchePage = trancheRepository.findAll(PageRequest.of(page, size));
        var trancheList = tranchePage.stream().map(trancheMapper::convert).toList();
        return PaginatedResponse.<TrancheResponse>builder()
                .result(trancheList)
                .page(tranchePage.getNumber())
                .size(tranchePage.getSize())
                .totalPage(tranchePage.getTotalPages())
                .totalItems(tranchePage.getTotalElements())
                .build();
    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void lockTranche(Long id) {
        var tranche = trancheRepository.findById(id).orElseThrow(() -> new AppException(ErrorCode.TRANCHE_NOT_FOUND));
        tranche.setStatus(TrancheStatus.LOCKED);
        trancheRepository.save(tranche);
    }
}
