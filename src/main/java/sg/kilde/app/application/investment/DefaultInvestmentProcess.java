package sg.kilde.app.application.investment;

import java.math.BigDecimal;
import sg.kilde.app.common.constant.ErrorCode;
import sg.kilde.app.common.exception.AppException;
import sg.kilde.app.domain.entity.Investment;
import sg.kilde.app.domain.repository.InvestmentRepository;

public class DefaultInvestmentProcess implements InvestmentProcess{

  private final InvestmentRepository investmentRepository;

  public DefaultInvestmentProcess(InvestmentRepository investmentRepository) {
    this.investmentRepository = investmentRepository;
  }

  @Override
  public Investment processInvestment(Investment investment) {
    var investmentList = investmentRepository.findByIdAndAppUserId(investment.getId(),
        investment.getAppUser().getId());

    BigDecimal totalInvestedAmount = BigDecimal.ZERO;
    for(var inv : investmentList){
      totalInvestedAmount = totalInvestedAmount.add(inv.getAmount());
    }

    totalInvestedAmount = totalInvestedAmount.add(investment.getAmount());
    if(totalInvestedAmount.compareTo(investment.getTranche().getMaximumInvestmentAmountPerInvestor()) > 0){
      throw new AppException(ErrorCode.INVESTMENT_AMOUNT_GREATER_THAN_MAX_INVESTMENT_PER_INVESTOR);
    }

    return investment;
  }

}

