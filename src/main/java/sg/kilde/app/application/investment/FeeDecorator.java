package sg.kilde.app.application.investment;

import java.math.BigDecimal;
import sg.kilde.app.common.utils.MathUtils;
import sg.kilde.app.domain.entity.Investment;
import sg.kilde.app.infrastracture.properties.AppFeeProperties;

public class FeeDecorator implements InvestmentProcess{
  private final InvestmentProcess investmentProcess;
  private final AppFeeProperties feeProperties;

  public FeeDecorator(InvestmentProcess investmentProcess, AppFeeProperties feeProperties) {
    this.investmentProcess = investmentProcess;
    this.feeProperties = feeProperties;
  }

  public Investment processInvestment(Investment investment) {
    investment = investmentProcess.processInvestment(investment);
    BigDecimal amount = MathUtils.reducePercentage(investment.getAmount(), feeProperties.getInvestmentFeePercent());
    investment.setAmount(amount);
    return investment;
  }

}
