package sg.kilde.app.application.investment;

import sg.kilde.app.domain.entity.Investment;

public interface InvestmentProcess {
  Investment processInvestment(Investment request);

}
