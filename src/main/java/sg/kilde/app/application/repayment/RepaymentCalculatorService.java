package sg.kilde.app.application.repayment;

import java.math.BigDecimal;
import java.math.RoundingMode;
import lombok.Getter;
import lombok.Setter;
import sg.kilde.app.common.utils.MathUtils;
import sg.kilde.app.domain.entity.Investment;
import sg.kilde.app.domain.entity.Tranche;

public class RepaymentCalculatorService {

  @Setter
  private BigDecimal borrowerFeePercentage;
  @Getter
  private BigDecimal monthlyRepayment;
  @Getter
  private BigDecimal lenderReturn;
  @Getter
  private BigDecimal borrowerFee;

  private RepaymentCalculatorService(BigDecimal borrowerFeePercentage){
    this.borrowerFeePercentage = borrowerFeePercentage;
  }

  public void calculate(Tranche tranche){
    BigDecimal monthlyInterestRateInDecimal = tranche.getAnnualInterestRate()
        .divide(new BigDecimal("12"), 4, RoundingMode.HALF_UP);

    // Apply the loan amortization formula
    BigDecimal powFactor = BigDecimal.ONE.add(monthlyInterestRateInDecimal).pow(tranche.getDurationInMonths());
    BigDecimal inversePowFactor = BigDecimal.ONE.divide(powFactor, powFactor.scale(), RoundingMode.HALF_UP);
    this.monthlyRepayment = tranche.getMaximumInvestmentAmount().multiply(monthlyInterestRateInDecimal)
        .divide(BigDecimal.ONE.subtract(inversePowFactor), RoundingMode.HALF_UP);

     this.borrowerFee = monthlyRepayment.multiply(MathUtils.convertPercentToDecimal(borrowerFeePercentage));

  }

  public void calculateLenderReturn(Investment investment){
    Tranche tranche = investment.getTranche();
    this.lenderReturn = investment.getAmount().divide(tranche.getMaximumInvestmentAmount(), 4, RoundingMode.HALF_DOWN)
        .multiply(monthlyRepayment);
  }

  public static RepaymentCalculatorService getInstance(BigDecimal borrowerFeePercentage){
    return new RepaymentCalculatorService(borrowerFeePercentage);
  }


}
