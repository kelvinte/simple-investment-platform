package sg.kilde.app.common.constant;

public class Constants {

  private Constants() {
  }

  public static final String JWT_HEADER = "Authorization";
  public static final String BEARER = "Bearer";
  public static final String AUTHORITY_CLAIM = "authorities";
  public static final String USER_ID_CLAIM = "userId";


}
