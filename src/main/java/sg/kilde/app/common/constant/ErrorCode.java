package sg.kilde.app.common.constant;

import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ProblemDetail;

public enum ErrorCode {
  INVALID_USERNAME_OR_PASSWORD("AUTH001", HttpStatus.FORBIDDEN, "Invalid Username or Password"),
  INVALID_TOKEN("AUTH002",HttpStatus.FORBIDDEN ,"Invalid Token Received" ),
  EMAIL_ALREADY_IN_USE("AUTH_003", HttpStatus.CONFLICT, "Email already in use"),
  MIN_INVESTMENT_GREATER_THAN_MAX_INVESTMENT("TN001",HttpStatus.BAD_REQUEST ,"Minimum investment is greater than maximum investment" ),
  MAX_INVESTMENT_PER_INVESTOR_LESS_THAN_MIN_INVESTMENT("TN002",HttpStatus.BAD_REQUEST ,"Maximum investment per investor is less than minimum investment" ),
  TRANCHE_NOT_FOUND("TN003",HttpStatus.NOT_FOUND ,"Tranche not found" ),
  INVESTMENT_MUST_BE_GREATER_THAN_ZERO("INV001",HttpStatus.BAD_REQUEST ,"Investment amount must be greater than zero" ),
  INVESTMENT_AMOUNT_GREATER_THAN_AVAILABLE_AMOUNT("INV002",HttpStatus.BAD_REQUEST ,"Investment amount greater than available amount" ),
  INVESTMENT_AMOUNT_GREATER_THAN_MAX_INVESTMENT_PER_INVESTOR("INV003",HttpStatus.BAD_REQUEST ,"Investment amount greater than maximum investment per investor" ),
  INVESTMENT_AMOUNT_LESS_THAN_MIN_INVESTMENT("INV004",HttpStatus.BAD_REQUEST ,"Investment amount less than minimum investment" ),
  TRANCHE_ALREADY_LOCKED("INV005",HttpStatus.BAD_REQUEST,"Tranche already locked, investments are not accepted anymore" ),
  USER_NOT_FOUND("USR001",HttpStatus.NOT_FOUND ,"User not found" ),
  REPAYMENT_NOT_FOUND("RP001",HttpStatus.NOT_FOUND ,"Repayment Not found" ),
  TRANCHE_NOT_LOCKED("RP002",HttpStatus.FORBIDDEN ,"Repayment not yet locked" );
  private String code;
  private HttpStatus statusCode;
  private String message;
  ErrorCode(String code, HttpStatus statusCode, String message) {
    this.code = code;
    this.statusCode = statusCode;
    this.message = message;
  }

  public String getCode() {
    return code;
  }

  public HttpStatusCode getStatusCode() {
    return statusCode;
  }

  public String getMessage() {
    return message;
  }

  public ProblemDetail getProblemDetail() {
    ProblemDetail problemDetail = ProblemDetail.forStatus(this.getStatusCode());
    problemDetail.setTitle(String.format("%s %s", this.getCode(), this.getMessage()));
    problemDetail.setProperty("success", false);
    problemDetail.setProperty("code", this.getCode());
    return problemDetail;
  }
}
