package sg.kilde.app.common.constant;

public class RoleConstants {

  private RoleConstants() {
  }

  public static final Integer ADMIN = 1;
  public static final Integer USER = 2;
}
