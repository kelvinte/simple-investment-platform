package sg.kilde.app.common.constant;

public enum TrancheStatus {
    OPEN, LOCKED, DONE
}
