package sg.kilde.app.common.constant;

public class UrlConstants {

  private UrlConstants() {
  }

  public static final String REGISTER = "/register";
  public static final String LOGIN = "/login";
  public static final String INVESTOR = "/investor";
  public static final String INVESTMENT = "/investment";
  public static final String ADMIN = "/admin";
  public static final String TRANCHE = "/tranche";
  public static final String ADMIN_TRANCHE = ADMIN + TRANCHE;

}
