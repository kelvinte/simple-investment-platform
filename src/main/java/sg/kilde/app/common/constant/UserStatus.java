package sg.kilde.app.common.constant;

public enum UserStatus {
  ACTIVE, INACTIVE, SUSPENDED
}
