package sg.kilde.app.common.exception;

import org.springframework.web.ErrorResponseException;
import sg.kilde.app.common.constant.ErrorCode;

public class AppException extends ErrorResponseException {
  public  AppException(ErrorCode exception) {
    super(exception.getStatusCode(), exception.getProblemDetail(), null);
  }

}
