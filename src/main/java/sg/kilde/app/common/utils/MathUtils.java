package sg.kilde.app.common.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class MathUtils {
  private MathUtils() {
  }

  public static BigDecimal convertPercentToDecimal(BigDecimal percent) {
    return percent.divide(BigDecimal.valueOf(100), 4, RoundingMode.DOWN);
  }

  public static BigDecimal reducePercentage(BigDecimal amount, BigDecimal percent) {
    return amount.multiply(BigDecimal.ONE.subtract(convertPercentToDecimal(percent)));
  }

  public static BigDecimal round(BigDecimal amount, int scale) {
    return amount.setScale(scale, RoundingMode.HALF_UP);
  }
}
