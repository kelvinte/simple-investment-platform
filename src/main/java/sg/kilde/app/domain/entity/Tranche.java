package sg.kilde.app.domain.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import java.math.BigDecimal;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import sg.kilde.app.common.constant.TrancheStatus;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "tranche")
public class Tranche {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "name", length = 255)
  private String name;

  @Column(name = "annual_interest_rate")
  private BigDecimal annualInterestRate;

  @Column(name = "available_amt", precision = 18, scale = 2)
  private BigDecimal availableAmount;

  @Column(name = "min_investment_amt", precision = 18, scale = 2)
  private BigDecimal minimumInvestmentAmount;

  @Column(name = "max_investment_amt", precision = 18, scale = 2)
  private BigDecimal maximumInvestmentAmount;

  @Column(name = "max_investment_amt_per_investor", precision = 18, scale = 2)
  private BigDecimal maximumInvestmentAmountPerInvestor;

  @Column(name = "duration_months")
  private int durationInMonths;

  @Column(name="status")
  @Enumerated(EnumType.STRING)
  private TrancheStatus status;

  @OneToMany(mappedBy = "tranche")
  private List<Repayment> repayments;

}