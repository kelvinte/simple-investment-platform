package sg.kilde.app.domain.repository;


import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import sg.kilde.app.domain.entity.AppUser;

public interface AppUserRepository extends JpaRepository<AppUser, Long> {
  Optional<AppUser> findByEmail(String email);
}
