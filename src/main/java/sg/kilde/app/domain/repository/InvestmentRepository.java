package sg.kilde.app.domain.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import sg.kilde.app.domain.entity.Investment;

public interface InvestmentRepository extends JpaRepository<Investment, Long> {

  List<Investment> findByIdAndAppUserId(Long investmentId, Long appUserId);

  List<Investment> findByAppUserId(Long appUserId);

  List<Investment> findByTrancheId(Long trancheId);
}
