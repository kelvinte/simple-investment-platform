package sg.kilde.app.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import sg.kilde.app.domain.entity.InvestmentReturn;

public interface InvestmentReturnRepository extends JpaRepository<InvestmentReturn, Long> {
}
