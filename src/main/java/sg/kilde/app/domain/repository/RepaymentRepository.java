package sg.kilde.app.domain.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import sg.kilde.app.domain.entity.Repayment;

public interface RepaymentRepository extends JpaRepository<Repayment, Long> {

  List<Repayment> findByTrancheId(Long trancheId);
}
