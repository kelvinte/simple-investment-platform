package sg.kilde.app.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import sg.kilde.app.domain.entity.Tranche;

public interface TrancheRepository extends PagingAndSortingRepository<Tranche,Long> , JpaRepository<Tranche, Long> {
}
