package sg.kilde.app.infrastracture.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import sg.kilde.app.application.investment.DefaultInvestmentProcess;
import sg.kilde.app.application.investment.FeeDecorator;
import sg.kilde.app.application.investment.InvestmentProcess;
import sg.kilde.app.domain.repository.InvestmentRepository;
import sg.kilde.app.infrastracture.properties.AppFeeProperties;

@Configuration
public class InvestmentProcessConfiguration {

  @Bean
  public InvestmentProcess investmentProcess(InvestmentRepository investmentRepository, AppFeeProperties feeProperties){
    return new FeeDecorator(new DefaultInvestmentProcess(investmentRepository), feeProperties);
  }

}
