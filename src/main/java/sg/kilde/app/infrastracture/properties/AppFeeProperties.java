package sg.kilde.app.infrastracture.properties;

import java.math.BigDecimal;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "app.fees")
public class AppFeeProperties {
  private BigDecimal borrowerFeePercent;
  private BigDecimal investmentFeePercent;

}
