-- liquibase formatted sql

-- changeset kelvin:1689226202

CREATE TABLE [app_role] (
  [id] int IDENTITY PRIMARY KEY,
  [name] varchar(45) NOT NULL,
  [description] varchar(255) NOT NULL
);


create table [app_user] (
    [id] bigint IDENTITY primary key,
    [email] varchar(255) not null,
    [password] varchar(255) not null,
    [name] varchar(255) not null,
    [status] varchar(10) not null,
    [app_role_id] int not null,
    [created_at] datetime default (GETDATE()),
    [updated_at] datetime,
     FOREIGN KEY ([app_role_id]) REFERENCES [app_role] ([id]),
);

CREATE UNIQUE INDEX app_user_email on app_user (email);


CREATE TABLE [tranche] (
    [id] bigint IDENTITY(1,1) PRIMARY KEY,
    [name] NVARCHAR(255),
    [annual_interest_rate]  DECIMAL(18, 6),
    [available_amt] DECIMAL(18, 2),
    [min_investment_amt] DECIMAL(18, 2),
    [max_investment_amt] DECIMAL(18, 2),
    [max_investment_amt_per_investor] DECIMAL(18, 2),
    [duration_months] INT,
    [status] VARCHAR(10) NOT NULL,
);

CREATE TABLE [investment] (
    [id] bigint IDENTITY(1,1) PRIMARY KEY,
    [app_user_id] BIGINT,
    [tranche_id] BIGINT,
    [amount] DECIMAL(18, 2),
    [created_at] DATETIME DEFAULT (GETDATE())
    FOREIGN KEY ([app_user_id]) REFERENCES [app_user] ([id]),
    FOREIGN KEY ([tranche_id]) REFERENCES [tranche] ([id])
);

CREATE TABLE [repayment] (
    [id] bigint IDENTITY(1,1) PRIMARY KEY,
    [tranche_id] BIGINT not null,
    [amount] DECIMAL(18, 2),
    [month_ordinal] INT,
    [paid] BIT,
    FOREIGN KEY ([tranche_id]) REFERENCES [tranche] ([id])
);

CREATE TABLE [investment_return] (
    [id] bigint IDENTITY(1,1) PRIMARY KEY,
    [investment_id] BIGINT not null,
    [amount] DECIMAL(18, 2),
    [month_ordinal] INT,
    FOREIGN KEY ([investment_id]) REFERENCES [investment] ([id])
);
