-- liquibase formatted sql

-- changeset kelvin:1689226202


SET IDENTITY_INSERT app_role ON;
INSERT INTO [app_role] ([id], [name], [description]) VALUES (1,'ADMIN', 'Admin role');
INSERT INTO [app_role] ([id], [name], [description]) VALUES (2,'USER', 'User role');
SET IDENTITY_INSERT app_role OFF;

SET IDENTITY_INSERT app_user ON
INSERT INTO [app_user] ([id],[email], [password], [name], [status], [app_role_id])
VALUES (1, 'admin@admin.com', '$2a$10$4WEVA0G6jmjwuYT9UT28B.SOpQucGE/w9xCCgoBWHiMc6Krhz4WK.', 'Admin', 'ACTIVE', 1)
SET IDENTITY_INSERT app_user OFF