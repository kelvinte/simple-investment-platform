package sg.kilde.app.adapter.incoming;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import sg.kilde.app.adapter.dto.auth.LoginRequest;
import sg.kilde.app.adapter.dto.auth.LoginResponse;
import sg.kilde.app.adapter.dto.auth.RegistrationRequest;
import sg.kilde.app.application.AuthenticationService;
import sg.kilde.app.infrastracture.configuration.SecurityConfiguration;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class AuthenticationControllerTest {

    @MockBean
    private AuthenticationService authenticationService;
    @Autowired
    private MockMvc mockMvc;
    @Test
    void validRegistration_register_noError() throws Exception {
        doNothing().when(authenticationService).register(any(RegistrationRequest.class));

        mockMvc.perform(post("/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(
                    """
                        {
                            "name": "Kelvin",
                            "email": "hello@gmail.com",
                            "password": "hello123"
                        }
                        """
                ))
                .andExpect(status().isOk());
    }

    @Test
    void validLogin_login_noError() throws Exception {
        LoginResponse loginResponse = new LoginResponse();
        loginResponse.setAccessToken("someacesstoken");
        when(authenticationService.login(any(LoginRequest.class))).thenReturn(loginResponse);

        mockMvc.perform(post("/login")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\n" +
                                "    \"email\":\"kelvinclarkte@gmail.com\",\n" +
                                "    \"password\":\"hello123\"\n" +
                                "}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data.accessToken").value("someacesstoken"));
    }
}