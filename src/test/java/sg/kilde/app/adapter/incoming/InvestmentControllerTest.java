package sg.kilde.app.adapter.incoming;

import java.math.BigDecimal;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import sg.kilde.app.adapter.dto.invest.InvestmentRequest;
import sg.kilde.app.adapter.dto.invest.InvestmentResponse;
import sg.kilde.app.adapter.dto.tranche.TrancheResponse;
import sg.kilde.app.application.AuthenticationService;
import sg.kilde.app.application.InvestmentService;
import sg.kilde.app.common.constant.TrancheStatus;
import sg.kilde.app.infrastracture.configuration.SecurityConfiguration;
import sg.kilde.app.testhelpers.TrancheCreator;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class InvestmentControllerTest {

    @MockBean
    private InvestmentService investmentService;

    @Autowired
    private WebApplicationContext context;

    private MockMvc mockMvc;

    @BeforeEach
    void before(){
        mockMvc = MockMvcBuilders.standaloneSetup(new InvestmentController(investmentService)).build();
    }
    @Test
    void validRequest_invest_noError() throws Exception {
        TrancheResponse trancheResponse = TrancheCreator.trancheResponseSupplier.get();
        InvestmentResponse investmentResponse = InvestmentResponse.builder()
            .amount(BigDecimal.valueOf(3000))
            .tranche(trancheResponse)
            .investmentId(1L)
            .build();
        when(investmentService.invest(any(InvestmentRequest.class)))
            .thenReturn(investmentResponse);


        mockMvc.perform(post("/investment")
                .contentType(MediaType.APPLICATION_JSON)
                .content(
                    """
                    {
                        "amount": "1000",
                        "trancheId": 1
                    }
                    """
                   ))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.amount").value(3000))
            .andExpect(jsonPath("$.investmentId").value(1))
            .andExpect(jsonPath("$.tranche.id").value(1))
            .andExpect(jsonPath("$.tranche.name").value("Tranche A"))
            .andExpect(jsonPath("$.tranche.status").value("OPEN"))
            .andExpect(jsonPath("$.tranche.annualInterestRate").value(3.0))
            .andExpect(jsonPath("$.tranche.availableAmount").value(1_000_000_000))
            .andExpect(jsonPath("$.tranche.minimumInvestmentAmount").value(1_000))
            .andExpect(jsonPath("$.tranche.maximumInvestmentAmount").value(1_000_000_000))
            .andExpect(jsonPath("$.tranche.maximumInvestmentAmountPerInvestor").value(1_000_000))
            .andExpect(jsonPath("$.tranche.durationInMonths").value(12));


    }
}