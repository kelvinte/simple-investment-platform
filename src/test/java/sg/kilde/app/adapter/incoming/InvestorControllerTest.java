package sg.kilde.app.adapter.incoming;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import sg.kilde.app.adapter.dto.invest.InvestmentResponse;
import sg.kilde.app.adapter.dto.investor.InvestorResponse;
import sg.kilde.app.adapter.dto.investor.InvestorTrancheResponse;
import sg.kilde.app.adapter.dto.tranche.TrancheResponse;
import sg.kilde.app.application.InvestorService;
import sg.kilde.app.common.constant.TrancheStatus;
import sg.kilde.app.testhelpers.TrancheCreator;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class InvestorControllerTest {

  @MockBean
  private InvestorService investorService;
  private MockMvc mockMvc;

  @BeforeEach
  void before(){
    mockMvc = MockMvcBuilders.standaloneSetup(new InvestorController(investorService)).build();
  }
  @Test
  void getInvestorDetails() throws Exception {

    TrancheResponse trancheResponse = TrancheCreator.trancheResponseSupplier.get();

    InvestmentResponse investmentResponse = InvestmentResponse.builder()
        .investmentId(1L)
        .amount(BigDecimal.valueOf(1_000))
        .tranche(trancheResponse)
        .build();

    InvestorTrancheResponse investorTrancheResponse = InvestorTrancheResponse.builder()
        .tranche(trancheResponse)
        .investments(List.of(investmentResponse))
        .build();
    InvestorResponse investorResponse = InvestorResponse.builder()
        .id(1L)
        .name("Investor A")
        .email("hello@gmail.com")
        .status("ACTIVE")
        .role("USER")
        .investments(List.of(investorTrancheResponse))
        .build();
    when(investorService.getInvestorDetails()).thenReturn(investorResponse);

    mockMvc.perform(get("/investor"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.id").value(1))
        .andExpect(jsonPath("$.name").value("Investor A"))
        .andExpect(jsonPath("$.email").value("hello@gmail.com"))
        .andExpect(jsonPath("$.status").value("ACTIVE"))
        .andExpect(jsonPath("$.role").value("USER"))
        .andExpect(jsonPath("$.investments[0].tranche.id").value(1))
        .andExpect(jsonPath("$.investments[0].tranche.name").value("Tranche A"))
        .andExpect(jsonPath("$.investments[0].tranche.status").value("OPEN"))
        .andExpect(jsonPath("$.investments[0].tranche.annualInterestRate").value(3.0))
        .andExpect(jsonPath("$.investments[0].tranche.durationInMonths").value(12))
        .andExpect(jsonPath("$.investments[0].tranche.minimumInvestmentAmount").value(1000))
        .andExpect(jsonPath("$.investments[0].tranche.maximumInvestmentAmount").value(1_000_000_000))
        .andExpect(jsonPath("$.investments[0].tranche.maximumInvestmentAmountPerInvestor").value(1_000_000))
        .andExpect(jsonPath("$.investments[0].tranche.availableAmount").value(1_000_000_000))
        .andExpect(jsonPath("$.investments[0].investments[0].investmentId").value(1))
        .andExpect(jsonPath("$.investments[0].investments[0].amount").value(1000));
  }
}