package sg.kilde.app.adapter.incoming;

import java.math.BigDecimal;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import sg.kilde.app.adapter.dto.repayment.RepaymentResponse;
import sg.kilde.app.application.RepaymentService;
import sg.kilde.app.testhelpers.RepaymentCreator;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class RepaymentControllerTest {

  @MockBean
  private RepaymentService repaymentService;

  private MockMvc mockMvc;

  @BeforeEach
  void before(){
    mockMvc = MockMvcBuilders.standaloneSetup(new RepaymentController(repaymentService)).build();
  }
  @Test
  void validTranche_pay_noError() throws Exception {
    RepaymentResponse repaymentResponse = RepaymentCreator.repaymentResponseSupplier.get();
    when(repaymentService.pay(anyLong(), anyLong())).thenReturn(repaymentResponse);
    mockMvc.perform(post("/admin/tranche/1/repayment/1"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.id").value(1L))
        .andExpect(jsonPath("$.amountToPay").value(1000L))
        .andExpect(jsonPath("$.month").value(1))
        .andExpect(jsonPath("$.paid").value(false));
  }
}