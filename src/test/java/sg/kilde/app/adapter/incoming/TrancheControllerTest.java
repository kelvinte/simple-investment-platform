package sg.kilde.app.adapter.incoming;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import sg.kilde.app.adapter.dto.common.PaginatedResponse;
import sg.kilde.app.adapter.dto.tranche.DetailedTrancheResponse;
import sg.kilde.app.adapter.dto.tranche.TrancheCreationRequest;
import sg.kilde.app.adapter.dto.tranche.TrancheResponse;
import sg.kilde.app.application.TrancheService;
import sg.kilde.app.testhelpers.RepaymentCreator;
import sg.kilde.app.testhelpers.TrancheCreator;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class TrancheControllerTest {

  @MockBean
  private TrancheService trancheService;

  private MockMvc mockMvc;

  @BeforeEach
  void before(){
    mockMvc = MockMvcBuilders.standaloneSetup(new TrancheController(trancheService)).build();
  }
  @Test
  void getPaginatedTranche() throws Exception {
    PaginatedResponse<TrancheResponse> paginatedResponse = new PaginatedResponse<>();
    paginatedResponse.setResult(List.of(TrancheCreator.trancheResponseSupplier.get()));
    paginatedResponse.setPage(1);
    paginatedResponse.setSize(10);
    paginatedResponse.setTotalItems(1L);
    paginatedResponse.setTotalPage(1);
    when(trancheService.getPaginatedTranches(anyInt(), anyInt())).thenReturn(paginatedResponse);

    mockMvc.perform(get("/tranche?page=1&size=10"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.page").value(1))
        .andExpect(jsonPath("$.size").value(10))
        .andExpect(jsonPath("$.totalItems").value(1))
        .andExpect(jsonPath("$.totalPage").value(1))
        .andExpect(jsonPath("$.result[0].id").value(1L))
        .andExpect(jsonPath("$.result[0].annualInterestRate").value(3.0))
        .andExpect(jsonPath("$.result[0].maximumInvestmentAmount").value(1_000_000_000))
        .andExpect(jsonPath("$.result[0].maximumInvestmentAmountPerInvestor").value(1_000_000))
        .andExpect(jsonPath("$.result[0].availableAmount").value(1_000_000_000))
        .andExpect(jsonPath("$.result[0].minimumInvestmentAmount").value(1_000))
        .andExpect(jsonPath("$.result[0].durationInMonths").value(12))
        .andExpect(jsonPath("$.result[0].name").value("Tranche A"))
        .andExpect(jsonPath("$.result[0].status").value("OPEN"));
  }

  @Test
  void getIndividualTranche() throws Exception {
    DetailedTrancheResponse detailedTrancheResponse = DetailedTrancheResponse.builder()
        .repayments(List.of(RepaymentCreator.repaymentResponseSupplier.get()))
        .tranche(TrancheCreator.trancheResponseSupplier.get())
        .build();
    when(trancheService.getTranche(anyLong())).thenReturn(detailedTrancheResponse);
    mockMvc.perform(get("/tranche/1"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.tranche.id").value(1L))
        .andExpect(jsonPath("$.tranche.annualInterestRate").value(3.0))
        .andExpect(jsonPath("$.tranche.maximumInvestmentAmount").value(1_000_000_000))
        .andExpect(jsonPath("$.tranche.maximumInvestmentAmountPerInvestor").value(1_000_000))
        .andExpect(jsonPath("$.tranche.availableAmount").value(1_000_000_000))
        .andExpect(jsonPath("$.tranche.minimumInvestmentAmount").value(1_000))
        .andExpect(jsonPath("$.tranche.durationInMonths").value(12))
        .andExpect(jsonPath("$.tranche.name").value("Tranche A"))
        .andExpect(jsonPath("$.tranche.status").value("OPEN"))
        .andExpect(jsonPath("$.repayments[0].id").value(1L))
        .andExpect(jsonPath("$.repayments[0].amountToPay").value(1_000))
        .andExpect(jsonPath("$.repayments[0].paid").value(false));
  }

  @Test
  void createTranche() throws Exception {
    DetailedTrancheResponse detailedTrancheResponse = DetailedTrancheResponse.builder()
        .repayments(List.of(RepaymentCreator.repaymentResponseSupplier.get()))
        .tranche(TrancheCreator.trancheResponseSupplier.get())
        .build();
    when(trancheService.createTranche(any(TrancheCreationRequest.class))).thenReturn(detailedTrancheResponse);
    mockMvc.perform(post("/admin/tranche")
            .content(
                """
                    {
                        "name":"Tranch A",
                        "annualInterestRateInPercent": "3",
                        "minimumInvestmentAmount": "100",
                        "maximumInvestmentAmount": "1000000000",
                        "maximumInvestmentAmountPerInvestor":"1000000",
                        "durationInMonths": 12
                    }
                    """
             )
            .contentType(MediaType.APPLICATION_JSON)
        )
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.tranche.id").value(1L))
        .andExpect(jsonPath("$.tranche.annualInterestRate").value(3.0))
        .andExpect(jsonPath("$.tranche.maximumInvestmentAmount").value(1_000_000_000))
        .andExpect(jsonPath("$.tranche.maximumInvestmentAmountPerInvestor").value(1_000_000))
        .andExpect(jsonPath("$.tranche.availableAmount").value(1_000_000_000))
        .andExpect(jsonPath("$.tranche.minimumInvestmentAmount").value(1_000))
        .andExpect(jsonPath("$.tranche.durationInMonths").value(12))
        .andExpect(jsonPath("$.tranche.name").value("Tranche A"))
        .andExpect(jsonPath("$.tranche.status").value("OPEN"))
        .andExpect(jsonPath("$.repayments[0].id").value(1L))
        .andExpect(jsonPath("$.repayments[0].amountToPay").value(1_000))
        .andExpect(jsonPath("$.repayments[0].paid").value(false));
  }

  @Test
  void lockTranche() throws Exception {
    doNothing().when(trancheService).lockTranche(anyLong());
    mockMvc.perform(patch("/admin/tranche/1/lock"))
        .andExpect(status().isOk());
  }
}