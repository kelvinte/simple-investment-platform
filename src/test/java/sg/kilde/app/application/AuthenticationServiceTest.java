package sg.kilde.app.application;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.crypto.password.PasswordEncoder;
import sg.kilde.app.adapter.dto.auth.LoginRequest;
import sg.kilde.app.adapter.dto.auth.RegistrationRequest;
import sg.kilde.app.common.constant.ErrorCode;
import sg.kilde.app.common.exception.AppException;
import sg.kilde.app.domain.entity.AppUser;
import sg.kilde.app.domain.repository.AppUserRepository;

@ExtendWith(MockitoExtension.class)
class AuthenticationServiceTest {

  @Mock
  private AppUserRepository appUserRepository;
  @Mock
  private PasswordEncoder passwordEncoder;
  @Mock
  private AuthenticationManager authenticationManager;
  @Mock
  private JWTTokenService jwtTokenService;
  @InjectMocks
  private AuthenticationService authenticationService;
  @Test
  void userExisting_register_throwError() {
    when(appUserRepository.findByEmail(anyString()))
        .thenReturn(Optional.of(AppUser.builder().email("Existing@exist.com")
            .build()));

    RegistrationRequest registrationRequest = RegistrationRequest.builder()
        .email("Existing@exist.com").build();
    assertThrows(AppException.class, () -> authenticationService.register(registrationRequest));

  }

  @Test
  void userNotExisting_register_success() {
    when(appUserRepository.save(any(AppUser.class))).thenReturn(AppUser.builder().build());
    when(appUserRepository.findByEmail(anyString()))
        .thenReturn(Optional.empty());
    when(passwordEncoder.encode(anyString())).thenReturn("encodedPassword");
    RegistrationRequest registrationRequest = RegistrationRequest.builder()
        .email("hello@hello.com")
        .name("hello")
        .password("hellopassword")
        .build();
    assertDoesNotThrow(() -> authenticationService.register(registrationRequest));

    ArgumentCaptor<String> passwordCaptor = ArgumentCaptor.forClass(String.class);
    verify(passwordEncoder).encode(passwordCaptor.capture());

    assertEquals("hellopassword", passwordCaptor.getValue());

    ArgumentCaptor<AppUser> appUserCaptor = ArgumentCaptor.forClass(AppUser.class);
    verify(appUserRepository).save(appUserCaptor.capture());
    AppUser appUser = appUserCaptor.getValue();
    assertEquals("hello@hello.com", appUser.getEmail());

  }
  @Test
  void existingUserCorrectCredential_login_success() {
    when(appUserRepository.findByEmail(anyString()))
        .thenReturn(Optional.of(AppUser.builder().email("hello@hello.com").build()));
    when(authenticationManager.authenticate(any())).thenReturn(null);
    when(jwtTokenService.generateToken(any())).thenReturn("token");

    var result = authenticationService.login(LoginRequest
                                  .builder()
                                  .email("hello@hello.com")
                                  .password("hellopassword")
                                  .build());
    assertEquals("token", result.getAccessToken());
  }

  @Test
  void existingUserIncorrectCredential_login_success() {
    when(appUserRepository.findByEmail(anyString()))
        .thenReturn(Optional.of(AppUser.builder().email("hello@hello.com").build()));
    when(authenticationManager.authenticate(any())).thenThrow(new BadCredentialsException(ErrorCode.INVALID_USERNAME_OR_PASSWORD.getMessage()));
    assertThrows(BadCredentialsException.class, () -> {
         authenticationService.login(LoginRequest
             .builder()
             .email("hello@hello.com")
             .password("hellopassword")
             .build());
       });
  }

  @Test
  void nonExistingUser_login_throwError() {
    when(appUserRepository.findByEmail(anyString()))
        .thenReturn(Optional.empty());
    assertThrows(BadCredentialsException.class, () -> {
      authenticationService.login(LoginRequest
          .builder()
          .email("hello@hello.com")
          .password("hellopassword")
          .build());
    });
  }
}