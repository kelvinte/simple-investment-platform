package sg.kilde.app.application;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import sg.kilde.app.adapter.dto.invest.InvestmentRequest;
import sg.kilde.app.adapter.mapper.InvestmentMapper;
import sg.kilde.app.adapter.mapper.InvestmentMapperImpl;
import sg.kilde.app.adapter.mapper.RepaymentMapperImpl;
import sg.kilde.app.adapter.mapper.TrancheMapperImpl;
import sg.kilde.app.application.investment.DefaultInvestmentProcess;
import sg.kilde.app.application.investment.FeeDecorator;
import sg.kilde.app.application.investment.InvestmentProcess;
import sg.kilde.app.common.constant.TrancheStatus;
import sg.kilde.app.common.exception.AppException;
import sg.kilde.app.domain.entity.AppUser;
import sg.kilde.app.domain.entity.Tranche;
import sg.kilde.app.domain.repository.AppUserRepository;
import sg.kilde.app.domain.repository.InvestmentRepository;
import sg.kilde.app.domain.repository.TrancheRepository;
import sg.kilde.app.domain.user.LoggedInUser;

@ExtendWith(MockitoExtension.class)
class InvestmentServiceTest {
  @Mock
  private TrancheRepository trancheRepository;
  @Mock
  private AppUserRepository appUserRepository;
  @Mock
  private InvestmentRepository investmentRepository;
  @Mock
  private LoggedinUserService loggedinUserService;
  @Mock
  private InvestmentProcess investmentProcess;
  private InvestmentMapper investmentMapper;
  private InvestmentService investmentService;
  @BeforeEach
  void setUp() {
    investmentMapper = new InvestmentMapperImpl(new TrancheMapperImpl(new RepaymentMapperImpl()));
    investmentService = new InvestmentService(trancheRepository, appUserRepository,
        investmentRepository, loggedinUserService, investmentProcess, investmentMapper);
  }
  @Test
  void trancheNotExisting_invest_throwError() {
    when(trancheRepository.findById(any())).thenReturn(Optional.empty());
    InvestmentRequest investmentRequest = InvestmentRequest.builder()
        .trancheId(1L)
        .amount(BigDecimal.valueOf(1000L))
        .build();
    assertThrows(AppException.class, () -> investmentService.invest(investmentRequest));

  }

  @Test
  void trancheLocked_invest_throwError() {
    when(trancheRepository.findById(any())).thenReturn(Optional.of(
        Tranche.builder()
        .status(TrancheStatus.LOCKED)
        .build()));
    InvestmentRequest investmentRequest = InvestmentRequest.builder()
        .trancheId(1L)
        .amount(BigDecimal.valueOf(1000L))
        .build();
    assertThrows(AppException.class, () -> investmentService.invest(investmentRequest));
  }

  @Test
  void trancheDone_invest_throwError() {
    when(trancheRepository.findById(any())).thenReturn(Optional.of(
        Tranche.builder()
            .status(TrancheStatus.DONE)
            .build()));
    InvestmentRequest investmentRequest = InvestmentRequest.builder()
        .trancheId(1L)
        .amount(BigDecimal.valueOf(1000L))
        .build();
    assertThrows(AppException.class, () -> investmentService.invest(investmentRequest));
  }

  @Test
  void investmentIsLessThanZero_invest_throwError(){
    when(trancheRepository.findById(any())).thenReturn(Optional.of(
        Tranche.builder()
            .status(TrancheStatus.OPEN)
            .build()));
    InvestmentRequest investmentRequest = InvestmentRequest.builder()
        .trancheId(1L)
        .amount(BigDecimal.valueOf(-1000L))
        .build();
    assertThrows(AppException.class, () -> investmentService.invest(investmentRequest));
  }

  @Test
  void investmentIsGreaterThanAvailableAmount_invest_throwError(){
    when(trancheRepository.findById(any())).thenReturn(Optional.of(
        Tranche.builder()
            .status(TrancheStatus.OPEN)
            .availableAmount(BigDecimal.valueOf(1000L))
            .build()));
    InvestmentRequest investmentRequest = InvestmentRequest.builder()
        .trancheId(1L)
        .amount(BigDecimal.valueOf(2000L))
        .build();
    assertThrows(AppException.class, () -> investmentService.invest(investmentRequest));
  }

  @Test
  void investmentIsLessThanMinimumAmount_invest_throwError(){
    when(trancheRepository.findById(any())).thenReturn(Optional.of(
        Tranche.builder()
            .status(TrancheStatus.OPEN)
            .maximumInvestmentAmountPerInvestor(BigDecimal.valueOf(10000L))
            .availableAmount(BigDecimal.valueOf(1000L))
            .minimumInvestmentAmount(BigDecimal.valueOf(100L))
            .build()));
    InvestmentRequest investmentRequest = InvestmentRequest.builder()
        .trancheId(1L)
        .amount(BigDecimal.valueOf(50))
        .build();
    assertThrows(AppException.class, () -> investmentService.invest(investmentRequest));
  }


  @Test
  void investmentIsMoreThanMaxAmountPerInvestor_invest_throwError(){
    when(trancheRepository.findById(any())).thenReturn(Optional.of(
        Tranche.builder()
            .status(TrancheStatus.OPEN)
            .maximumInvestmentAmountPerInvestor(BigDecimal.valueOf(10000L))
            .availableAmount(BigDecimal.valueOf(100000L))
            .build()));
    InvestmentRequest investmentRequest = InvestmentRequest.builder()
        .trancheId(1L)
        .amount(BigDecimal.valueOf(20000))
        .build();
    assertThrows(AppException.class, () -> investmentService.invest(investmentRequest));
  }

  @Test
  void investmentIsValid_invest_returnSuccess(){
    when(trancheRepository.findById(any())).thenReturn(Optional.of(
        Tranche.builder()
            .status(TrancheStatus.OPEN)
            .minimumInvestmentAmount(BigDecimal.ONE)
            .maximumInvestmentAmountPerInvestor(BigDecimal.valueOf(100L))
            .availableAmount(BigDecimal.valueOf(100000L))
            .build()));
    LoggedInUser loggedinUser = new LoggedInUser(1L, "test", "test");
    when(loggedinUserService.getLoggedInUser()).thenReturn(loggedinUser);
    when(appUserRepository.findById(any())).thenReturn(Optional.of(
        AppUser.builder()
            .id(1L)
            .build()));
    when(investmentRepository.save(any())).then(invocationOnMock -> invocationOnMock.getArgument(0));
    when(investmentProcess.processInvestment(any())).then(invocationOnMock -> invocationOnMock.getArgument(0));
    when(trancheRepository.save(any())).then(invocationOnMock -> invocationOnMock.getArgument(0));
    InvestmentRequest investmentRequest = InvestmentRequest.builder()
        .trancheId(1L)
        .amount(BigDecimal.valueOf(10))
        .build();
    assertDoesNotThrow(() -> investmentService.invest(investmentRequest));
  }
}