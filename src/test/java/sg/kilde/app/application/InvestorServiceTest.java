package sg.kilde.app.application;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;
import sg.kilde.app.adapter.mapper.InvestmentMapper;
import sg.kilde.app.adapter.mapper.InvestmentMapperImpl;
import sg.kilde.app.adapter.mapper.InvestorMapper;
import sg.kilde.app.adapter.mapper.InvestorMapperImpl;
import sg.kilde.app.adapter.mapper.RepaymentMapperImpl;
import sg.kilde.app.adapter.mapper.TrancheMapper;
import sg.kilde.app.adapter.mapper.TrancheMapperImpl;
import sg.kilde.app.common.constant.UserStatus;
import sg.kilde.app.common.exception.AppException;
import sg.kilde.app.domain.entity.AppRole;
import sg.kilde.app.domain.entity.AppUser;
import sg.kilde.app.domain.entity.Investment;
import sg.kilde.app.domain.repository.AppUserRepository;
import sg.kilde.app.domain.repository.InvestmentRepository;
import sg.kilde.app.domain.user.LoggedInUser;
import sg.kilde.app.testhelpers.RepaymentCreator;
import sg.kilde.app.testhelpers.TrancheCreator;

@ExtendWith(MockitoExtension.class)
class InvestorServiceTest {


  @Mock
  private LoggedinUserService loggedinUserService;
  @Mock
  private AppUserRepository appUserRepository;
  @Mock
  private InvestmentRepository investmentRepository;


  private InvestorMapper investorMapper;

  private InvestorService investorService;
  @BeforeEach
  void setUp() {
    investorMapper = new InvestorMapperImpl();
    TrancheMapper trancheMapper = new TrancheMapperImpl(new RepaymentMapperImpl());
    InvestmentMapper investmentMapper = new InvestmentMapperImpl(trancheMapper);
    ReflectionTestUtils.setField(investorMapper, "investmentMapper", investmentMapper);
    ReflectionTestUtils.setField(investorMapper, "trancheMapper", trancheMapper);
    investorService = new InvestorService(loggedinUserService, appUserRepository, investmentRepository, investorMapper);
  }
  @Test
  void userNotExisting_getInvestorDetail_throwError() {
    LoggedInUser loggedInUser = new LoggedInUser(1L, "test", "test", null);
    when(loggedinUserService.getLoggedInUser()).thenReturn(loggedInUser);
    when(appUserRepository.findById(loggedInUser.getId())).thenReturn(Optional.empty());
    assertThrows(AppException.class, () -> investorService.getInvestorDetails());
  }

  @Test
  void userExisting_getInvestorDetail_returnInvestor() {
    LoggedInUser loggedInUser = new LoggedInUser(1L, "test", "test", null);
    when(loggedinUserService.getLoggedInUser()).thenReturn(loggedInUser);
    AppUser appUser = new AppUser();
    appUser.setId(1L);
    appUser.setAppRole(AppRole.builder().name("USER").build());
    appUser.setName("hello");
    appUser.setStatus(UserStatus.ACTIVE);

    when(appUserRepository.findById(eq(1L))).thenReturn(Optional.of(appUser));

    var tranche = TrancheCreator.trancheSupplier.get();
    tranche.setRepayments(List.of(RepaymentCreator.repaymentSupplier.get()));
    Investment investment = Investment.builder()
        .appUser(appUser)
        .tranche(tranche)
        .amount(BigDecimal.valueOf(1000L))
        .id(1L)
        .build();
    when(investmentRepository.findByAppUserId(eq(1L))).thenReturn(List.of(investment));
    var investorDetails = investorService.getInvestorDetails();
    assertEquals(1, investorDetails.getInvestments().size());
    assertEquals("hello", investorDetails.getName());
    assertEquals("USER", investorDetails.getRole());
    assertEquals("ACTIVE", investorDetails.getStatus());
    assertEquals(BigDecimal.valueOf(1000),
        investorDetails.getInvestments().get(0).getInvestments().get(0)
            .getAmount().setScale(0, RoundingMode.DOWN));
  }
}