package sg.kilde.app.application;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.test.util.ReflectionTestUtils;
import sg.kilde.app.domain.entity.AppRole;
import sg.kilde.app.domain.entity.AppUser;


class JWTTokenServiceTest {

  private JWTTokenService jwtTokenService;

  private String token;
  @BeforeEach
  void setUp() {
    jwtTokenService = new JWTTokenService();
    ReflectionTestUtils.setField(jwtTokenService,"jwtSecretKey","VquHiRGQLFPdGSj7V9GmnCamgeOtAXuh" );
    ReflectionTestUtils.setField(jwtTokenService,"jwtExpiration",86400000 );
  }

  @Test
  void generateToken() {
    AppUser appUser = new AppUser();
    appUser.setId(1L);
    appUser.setEmail("test@test.com");
    appUser.setName("test");
    appUser.setAppRole(AppRole.builder().name("USER").build());

    token = jwtTokenService.generateToken(appUser);
    assertNotNull(token);

    String username = jwtTokenService.extractUsername(token);
    assertEquals("test@test.com", username);

  }

}