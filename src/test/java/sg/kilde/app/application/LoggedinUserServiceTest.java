package sg.kilde.app.application;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import sg.kilde.app.domain.user.LoggedInUser;

@ExtendWith(SpringExtension.class)
class LoggedinUserServiceTest {


  private LoggedinUserService loggedinUserService;
  @Test

  void getLoggedInUser() {
    var sch = Mockito.mockStatic(SecurityContextHolder.class);
    SecurityContext securityContext = Mockito.mock(SecurityContext.class);
    sch.when(SecurityContextHolder::getContext).thenReturn(securityContext);
    LoggedInUser loggedInUser = new LoggedInUser(1L, "test", null);
    when(securityContext.getAuthentication()).thenReturn(loggedInUser);

    loggedinUserService = new LoggedinUserService();
    var user = loggedinUserService.getLoggedInUser();
    assertEquals(1L, user.getId());
  }
}