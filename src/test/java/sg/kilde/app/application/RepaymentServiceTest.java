package sg.kilde.app.application;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import sg.kilde.app.adapter.dto.tranche.TrancheCreationRequest;
import sg.kilde.app.adapter.mapper.RepaymentMapper;
import sg.kilde.app.adapter.mapper.RepaymentMapperImpl;
import sg.kilde.app.common.constant.TrancheStatus;
import sg.kilde.app.common.exception.AppException;
import sg.kilde.app.domain.entity.Investment;
import sg.kilde.app.domain.entity.Repayment;
import sg.kilde.app.domain.entity.Tranche;
import sg.kilde.app.domain.repository.InvestmentRepository;
import sg.kilde.app.domain.repository.InvestmentReturnRepository;
import sg.kilde.app.domain.repository.RepaymentRepository;
import sg.kilde.app.infrastracture.properties.AppFeeProperties;
import sg.kilde.app.testhelpers.TrancheCreator;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class RepaymentServiceTest {

    @Mock
    private RepaymentRepository repaymentRepository;

    @Mock
    private InvestmentRepository investmentRepository;
    private RepaymentMapper repaymentMapper;

    @Mock
    private InvestmentReturnRepository investmentReturnRepository;

    private AppFeeProperties appFeeProperties;

    private RepaymentService repaymentService;

    @BeforeEach
    void setUp() {
        this.repaymentMapper = new RepaymentMapperImpl();
        this.appFeeProperties = new AppFeeProperties();
        appFeeProperties.setBorrowerFeePercent(BigDecimal.valueOf(2));
        appFeeProperties.setInvestmentFeePercent(BigDecimal.valueOf(2));
        repaymentService = new RepaymentService(repaymentRepository, investmentRepository, repaymentMapper,
                investmentReturnRepository, appFeeProperties);

    }
    @Test
    void repaymentByTrancheIdNotExisting_pay_throwException() {
        when(repaymentRepository.findByTrancheId(anyLong())).thenReturn(null);
        assertThrows(AppException.class, () -> repaymentService.pay(1L, 1L));
    }

    @Test
    void repaymentIdNotExisting_pay_throwException() {
        Repayment repayment = Repayment.builder()
                .id(2L)
                .build();
        when(repaymentRepository.findByTrancheId(anyLong())).thenReturn(List.of(repayment));
        assertThrows(AppException.class, () -> repaymentService.pay(1L, 1L));

    }

    @Test
    void trancheNotLock_pay_throwException(){
        Tranche tranche = TrancheCreator.trancheSupplier.get();
        tranche.setStatus(TrancheStatus.OPEN);
        Repayment repayment = Repayment.builder()
                .id(1L)
                .tranche(tranche)
                .build();
        when(repaymentRepository.findByTrancheId(anyLong())).thenReturn(List.of(repayment));
        assertThrows(AppException.class, () -> repaymentService.pay(1L, 1L));

    }

    @Test
    void trancheLocked_pay_success(){
        Tranche tranche = TrancheCreator.trancheSupplier.get();
        tranche.setStatus(TrancheStatus.LOCKED);
        Repayment repayment = Repayment.builder()
                .id(1L)
                .tranche(tranche)
                .monthOrdinal(1)
                .amount(BigDecimal.valueOf(1_000))
                .build();
        when(repaymentRepository.findByTrancheId(anyLong())).thenReturn(List.of(repayment));
        when(repaymentRepository.save(repayment)).then(invocation -> invocation.getArgument(0));

        Investment investment = Investment.builder()
                .id(1L)
                .amount(BigDecimal.valueOf(10_000))
                .tranche(tranche)
                .build();
        when(investmentRepository.findByTrancheId(anyLong())).thenReturn(List.of(investment));
        when(investmentReturnRepository.saveAll(anyList())).then(invocation -> invocation.getArgument(0));

        repaymentService.pay(1L, 1L);

        ArgumentCaptor<Repayment> repaymentArgumentCaptor = ArgumentCaptor.forClass(Repayment.class);
        verify(repaymentRepository).save(repaymentArgumentCaptor.capture());

        var repaymentArgumentCaptorValue = repaymentArgumentCaptor.getValue();
        assertTrue(repaymentArgumentCaptorValue.getPaid());
        assertEquals(TrancheStatus.DONE, repaymentArgumentCaptorValue.getTranche().getStatus());
        assertEquals(1, repaymentArgumentCaptorValue.getId());
    }
}
