package sg.kilde.app.application;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import sg.kilde.app.adapter.dto.tranche.TrancheCreationRequest;
import sg.kilde.app.adapter.mapper.RepaymentMapperImpl;
import sg.kilde.app.adapter.mapper.TrancheMapper;
import sg.kilde.app.adapter.mapper.TrancheMapperImpl;
import sg.kilde.app.common.constant.TrancheStatus;
import sg.kilde.app.common.exception.AppException;
import sg.kilde.app.domain.entity.Repayment;
import sg.kilde.app.domain.entity.Tranche;
import sg.kilde.app.domain.repository.RepaymentRepository;
import sg.kilde.app.domain.repository.TrancheRepository;
import sg.kilde.app.infrastracture.properties.AppFeeProperties;
import sg.kilde.app.testhelpers.TrancheCreator;

import java.awt.print.Pageable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class TrancheServiceTest {

    private TrancheMapper trancheMapper;
    @Mock
    private TrancheRepository trancheRepository;
    @Mock
    private RepaymentRepository repaymentRepository;
    private  AppFeeProperties appFeeProperties;

    private TrancheService trancheService;

    @BeforeEach
    void setup(){
        var repaymentMapper = new RepaymentMapperImpl();
        trancheMapper = new TrancheMapperImpl(repaymentMapper);

        appFeeProperties = new AppFeeProperties();
        appFeeProperties.setBorrowerFeePercent(BigDecimal.valueOf(2));
        appFeeProperties.setInvestmentFeePercent(BigDecimal.valueOf(2));

        trancheService = new TrancheService(trancheMapper, trancheRepository, repaymentRepository, appFeeProperties);
    }

    @Test
    void trancheNotExisting_getTranche_throwError() {
        when(trancheRepository.findById(anyLong())).thenReturn(Optional.empty());
        assertThrows(AppException.class, () -> trancheService.getTranche(1L));
    }

    @Test
    void trancheExisting_getTranche_success(){
        when(trancheRepository.findById(anyLong())).thenReturn(Optional.of(TrancheCreator.trancheSupplier.get()));
        var tranche = trancheService.getTranche(1L);
        assertNotNull(tranche);
        assertEquals(1L, tranche.getTranche().getId());
        assertEquals(BigDecimal.valueOf(3.0), tranche.getTranche().getAnnualInterestRate());
        assertEquals(BigDecimal.valueOf(1_000_000_000), tranche.getTranche().getMaximumInvestmentAmount());
        assertEquals(BigDecimal.valueOf(1_000_000), tranche.getTranche().getMaximumInvestmentAmountPerInvestor());
        assertEquals(BigDecimal.valueOf(1_000_000_000), tranche.getTranche().getAvailableAmount());

    }
    @Test
    void minInvestmentIsGreaterThanMaxInvestment_createTranche_error() {
        var trancheCreationRequest = new TrancheCreationRequest();
        trancheCreationRequest.setMaximumInvestmentAmount(BigDecimal.valueOf(1));
        trancheCreationRequest.setMinimumInvestmentAmount(BigDecimal.valueOf(2));
        assertThrows(AppException.class, () -> trancheService.createTranche(trancheCreationRequest));
    }

    @Test
    void maxInvestPerPersonLessThanMinInvestment_createTranche_error() {
        var trancheCreationRequest = new TrancheCreationRequest();
        trancheCreationRequest.setMaximumInvestmentAmount(BigDecimal.valueOf(100));
        trancheCreationRequest.setMaximumInvestmentAmountPerInvestor(BigDecimal.valueOf(1));
        trancheCreationRequest.setMinimumInvestmentAmount(BigDecimal.valueOf(2));
        assertThrows(AppException.class, () -> trancheService.createTranche(trancheCreationRequest));
    }

    @Test
    void validInput_createTranche_mustCreateRepaymentData(){
        var trancheCreationRequest = new TrancheCreationRequest();
        trancheCreationRequest.setMaximumInvestmentAmount(BigDecimal.valueOf(100_000));
        trancheCreationRequest.setMaximumInvestmentAmountPerInvestor(BigDecimal.valueOf(1_000));
        trancheCreationRequest.setMinimumInvestmentAmount(BigDecimal.valueOf(100));
        trancheCreationRequest.setDurationInMonths(12);
        trancheCreationRequest.setAnnualInterestRateInPercent(BigDecimal.valueOf(3.0));
        trancheCreationRequest.setName("Tranche A");

        when(trancheRepository.save(any(Tranche.class))).then(invocation -> invocation.getArgument(0));

        when(repaymentRepository.saveAll(any())).then(invocation -> invocation.getArgument(0));

        var tranche = trancheService.createTranche(trancheCreationRequest);
        assertNotNull(tranche);
        assertEquals(0, BigDecimal.valueOf(0.0300).compareTo(tranche.getTranche().getAnnualInterestRate()));
        assertEquals(BigDecimal.valueOf(100_000), tranche.getTranche().getMaximumInvestmentAmount());
        assertEquals(BigDecimal.valueOf(1000), tranche.getTranche().getMaximumInvestmentAmountPerInvestor());
        assertEquals(BigDecimal.valueOf(100_000), tranche.getTranche().getAvailableAmount());
        assertEquals(12, tranche.getTranche().getDurationInMonths());
        assertEquals("Tranche A", tranche.getTranche().getName());
        assertEquals(12, tranche.getRepayments().size());


        ArgumentCaptor<List<Repayment>> captor = ArgumentCaptor.forClass(List.class);
        verify(repaymentRepository).saveAll(captor.capture());

        assertEquals(12, captor.getValue().size());
    }
    @Test
    void getPaginatedTranches() {
        PageRequest pageRequest = PageRequest.of(0,10);
        Page<Tranche> page = new PageImpl<>(List.of(TrancheCreator.trancheSupplier.get()), pageRequest, 1);
        when(trancheRepository.findAll(any(PageRequest.class))).thenReturn(page);
        var result = trancheService.getPaginatedTranches(0,10);
        assertNotNull(result);
        assertEquals(1, result.getResult().size());
        assertEquals(1, result.getTotalPage());
        assertEquals(1, result.getTotalItems());
        assertEquals(1, result.getResult().get(0).getId());
        assertEquals("Tranche A", result.getResult().get(0).getName());
    }

    @Test
    void nonExistingTranche_lock_error() {
        when(trancheRepository.findById(anyLong())).thenReturn(Optional.empty());
        assertThrows(AppException.class, () -> trancheService.lockTranche(1L));
    }

    @Test
    void tranchExisting_lock_success() {
        var tranche = TrancheCreator.trancheSupplier.get();
        when(trancheRepository.findById(anyLong())).thenReturn(Optional.of(tranche));
        when(trancheRepository.save(any(Tranche.class))).then(invocation -> invocation.getArgument(0));
        trancheService.lockTranche(1L);

        ArgumentCaptor<Tranche> captor = ArgumentCaptor.forClass(Tranche.class);
        verify(trancheRepository).save(captor.capture());
        assertEquals(TrancheStatus.LOCKED, captor.getValue().getStatus());
    }

}