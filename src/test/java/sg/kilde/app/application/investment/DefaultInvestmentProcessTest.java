package sg.kilde.app.application.investment;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import sg.kilde.app.common.exception.AppException;
import sg.kilde.app.domain.entity.AppUser;
import sg.kilde.app.domain.entity.Investment;
import sg.kilde.app.domain.entity.Tranche;
import sg.kilde.app.domain.repository.InvestmentRepository;

@ExtendWith(MockitoExtension.class)
class DefaultInvestmentProcessTest {

  @Mock
  private InvestmentRepository investmentRepository;

  @InjectMocks
  private DefaultInvestmentProcess defaultInvestmentProcess;

  @Test
  void exceedMaxInvestedAmount_invest_throwError() {
    Investment investment = Investment.builder()
        .amount(BigDecimal.valueOf(1000))
        .build();
    when(investmentRepository.findByIdAndAppUserId(anyLong(), anyLong()))
        .thenReturn(List.of(investment));

    Tranche tranche = Tranche.builder()
        .maximumInvestmentAmountPerInvestor(BigDecimal.valueOf(1000))
        .build();

    Investment newInvestment = Investment.builder()
        .tranche(tranche)
        .appUser(AppUser.builder().id(1L).build())
        .amount(BigDecimal.valueOf(2000))
        .id(1L)
        .build();

    assertThrows(AppException.class, ()->{
      defaultInvestmentProcess.processInvestment(newInvestment);
    });
  }

  @Test
  void investmentIsBelowLimit_invest_noError() {
    Investment investment = Investment.builder()
        .amount(BigDecimal.valueOf(1000))
        .build();
    when(investmentRepository.findByIdAndAppUserId(anyLong(), anyLong()))
        .thenReturn(List.of(investment));

    Tranche tranche = Tranche.builder()
        .maximumInvestmentAmountPerInvestor(BigDecimal.valueOf(3000))
        .build();

    Investment newInvestment = Investment.builder()
        .tranche(tranche)
        .appUser(AppUser.builder().id(1L).build())
        .amount(BigDecimal.valueOf(2000))
        .id(1L)
        .build();


     Investment result = defaultInvestmentProcess
         .processInvestment(newInvestment);

     assertNotNull(result);
     assertEquals(1L, result.getId());
     assertEquals(BigDecimal.valueOf(2000), result.getAmount());


  }
}