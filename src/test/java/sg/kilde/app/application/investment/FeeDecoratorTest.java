package sg.kilde.app.application.investment;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.math.RoundingMode;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import sg.kilde.app.domain.entity.Investment;
import sg.kilde.app.infrastracture.properties.AppFeeProperties;

@ExtendWith(MockitoExtension.class)
class FeeDecoratorTest {

  @Mock
  private InvestmentProcess investmentProcess;
  private AppFeeProperties feeProperties;

  private FeeDecorator feeDecorator;

  @BeforeEach
  void setUp() {
    feeProperties = new AppFeeProperties();
    feeProperties.setInvestmentFeePercent(BigDecimal.valueOf(2));
    feeProperties.setBorrowerFeePercent(BigDecimal.valueOf(2));

    feeDecorator = new FeeDecorator(investmentProcess, feeProperties);
  }
  @Test
  void processInvestment() {
    when(investmentProcess.processInvestment(any(Investment.class)))
        .thenReturn(Investment.builder().amount(BigDecimal.valueOf(1000)).build());

    Investment investment = feeDecorator.processInvestment(new Investment());

    assertEquals(BigDecimal.valueOf(980).setScale(2, RoundingMode.DOWN),
        investment.getAmount().setScale(2, RoundingMode.DOWN));
  }
}