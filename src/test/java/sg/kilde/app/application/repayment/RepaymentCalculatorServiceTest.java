package sg.kilde.app.application.repayment;

import static org.junit.jupiter.api.Assertions.*;

import jakarta.persistence.criteria.CriteriaBuilder.In;
import java.math.BigDecimal;
import java.math.RoundingMode;
import org.junit.jupiter.api.Test;
import sg.kilde.app.domain.entity.Investment;
import sg.kilde.app.domain.entity.Tranche;


class RepaymentCalculatorServiceTest {

  @Test
  void calculate() {
    RepaymentCalculatorService repaymentCalculatorService =
        RepaymentCalculatorService.getInstance(BigDecimal.valueOf(2));

    Tranche tranche = Tranche.builder()
        .durationInMonths(12)
        .annualInterestRate(BigDecimal.valueOf(0.06))
        .maximumInvestmentAmount(BigDecimal.valueOf(1000))
        .build();

    repaymentCalculatorService.calculate(tranche);
    assertNotNull(repaymentCalculatorService.getMonthlyRepayment());
    assertEquals("86.0664", repaymentCalculatorService.getMonthlyRepayment().toPlainString());

  }

  @Test
  void calculateLenderReturn() {
    RepaymentCalculatorService repaymentCalculatorService =
        RepaymentCalculatorService.getInstance(BigDecimal.valueOf(2));

    Tranche tranche = Tranche.builder()
        .durationInMonths(12)
        .annualInterestRate(BigDecimal.valueOf(0.06))
        .maximumInvestmentAmount(BigDecimal.valueOf(1000))
        .build();

    repaymentCalculatorService.calculate(tranche);

    Investment investment = Investment.builder()
        .tranche(tranche)
        .amount(BigDecimal.valueOf(1000))
        .build();
    repaymentCalculatorService.calculateLenderReturn(investment);
    assertNotNull(repaymentCalculatorService.getLenderReturn());
    assertEquals("86.0664", repaymentCalculatorService
        .getLenderReturn().setScale(4, RoundingMode.DOWN).toPlainString());

  }
}