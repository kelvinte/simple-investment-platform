package sg.kilde.app.testhelpers;

import java.math.BigDecimal;
import java.util.function.Supplier;
import sg.kilde.app.adapter.dto.repayment.RepaymentResponse;
import sg.kilde.app.adapter.dto.tranche.TrancheResponse;
import sg.kilde.app.domain.entity.Repayment;

public class RepaymentCreator {
  public static Supplier<RepaymentResponse> repaymentResponseSupplier = () ->RepaymentResponse.builder()
      .id(1L)
      .amountToPay(BigDecimal.valueOf(1000L))
      .month(1)
      .paid(false)
      .build();

  public static Supplier<Repayment> repaymentSupplier = () -> Repayment.builder()
      .id(1L)
      .amount(BigDecimal.valueOf(1000L))
      .monthOrdinal(1)
      .paid(false)
      .build();
}
