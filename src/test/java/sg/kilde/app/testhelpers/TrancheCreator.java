package sg.kilde.app.testhelpers;

import java.math.BigDecimal;
import java.util.function.Supplier;
import sg.kilde.app.adapter.dto.tranche.TrancheResponse;
import sg.kilde.app.common.constant.TrancheStatus;
import sg.kilde.app.domain.entity.Tranche;

public class TrancheCreator {
  public static Supplier<TrancheResponse> trancheResponseSupplier = () -> TrancheResponse.builder()
      .id(1)
      .annualInterestRate(BigDecimal.valueOf(3.0))
      .maximumInvestmentAmount(BigDecimal.valueOf(1_000_000_000))
      .maximumInvestmentAmountPerInvestor(BigDecimal.valueOf(1_000_000))
      .availableAmount(BigDecimal.valueOf(1_000_000_000))
      .minimumInvestmentAmount(BigDecimal.valueOf(1_000))
      .durationInMonths(12)
      .name("Tranche A")
      .status(TrancheStatus.OPEN.name())
      .build();

  public static Supplier<Tranche> trancheSupplier = () -> Tranche.builder()
      .id(1L)
      .annualInterestRate(BigDecimal.valueOf(3.0))
      .maximumInvestmentAmount(BigDecimal.valueOf(1_000_000_000))
      .maximumInvestmentAmountPerInvestor(BigDecimal.valueOf(1_000_000))
      .availableAmount(BigDecimal.valueOf(1_000_000_000))
      .minimumInvestmentAmount(BigDecimal.valueOf(1_000))
      .durationInMonths(12)
      .name("Tranche A")
      .status(TrancheStatus.OPEN)
      .build();



}
